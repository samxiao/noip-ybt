#include <bits/stdc++.h>
using namespace std;

void del1(char *s, int cnt) {
   int pos = 0;
   while (s[pos] <= s[pos + 1] && pos < cnt - 1) pos++;
   if (pos != cnt - 1)
      for (int i = pos; i < cnt - 1; i++) s[i] = s[i + 1];

   s[cnt - 1] = 0;
}

void getN(long long n, int k) {
   char s[100] = {0}, cnt;
   memset(s, 0, sizeof(s));
   cnt = sprintf(s, "%lld", n);

   while (k--) {
      del1(s, cnt--);
   }
   printf("%s\n", s);
}

void work1() {
   int n;
   scanf("%d", &n);

   for (int i = 1; i <= n; i++) {
      long long x;
      int k;
      scanf("%lld%d", &x, &k);
      getN(x, k);
   }
}

int main() {
   work1();
   return 0;
}
