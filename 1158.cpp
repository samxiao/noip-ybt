//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int sum(int x )
{
	if(1 == x) return x;
	return x + sum(x - 1);
}

void work1()
{
	int n;
	scanf("%d", &n);
	printf("%d", sum(n));
}

int main()
{
	work1();
  return 0;
}
