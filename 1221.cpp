#include <bits/stdc++.h>
using namespace std;
//6OK, 4WA
#define szARR_CNT(x) (sizeof(x) / sizeof(x[0]))
int a[100];
int g[100];

int n;
int gcd(int a, int b) {
   if (0 == a % b) return b;
   return gcd(b, a % b);
}

bool canAdd(int pos, int gNo) {
   if (g[pos]) return false;
   for (int k = 1; k <= n; k++) {
      if (g[k] == g[gNo]) {
         if (1 != gcd(a[k], a[pos])) return false;
      }
   }
   return true;
}

int ans;
void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      scanf("%d", &a[i]);
   }
   for (int i = 1; i <= n; i++) {
      if (!g[i]) {
         g[i] = i;
         ans++;
      } //else continue;
      for (int gNo = 1; gNo <= i; gNo++) {
         for (int j = i + 1; j <= n; j++) {
            if (canAdd(j, gNo)) g[j] = g[gNo];
         }
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
