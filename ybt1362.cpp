#include <iostream>
#include <bits/stdc++.h>

using namespace std;

#define szARR(x) sizeof(x) / sizeof(x[0])
#define DBG_PRINTF
#define MAXN 10000
int f[MAXN];

void init(int n)
{
	for(int i=1; i<=n; i++) f[i] = i;
}

int find(int x)
{
	return x == f[x] ? x : f[x] = find(f[x]);
}

void joinn(int a, int b)
{
	int x, y;
	x = find(a);
	y = find(b);
	if(x != y) f[y] = x;
}

bool judge(int a, int b)
{
	return find(a) == find(b);
}

int main()
{
	int n, k, ans = 0, x, y;
	cin >> n >> k;

	init(n);
	for (int i = 0; i < k; i++)
	{
		cin >> x >> y;
		joinn(x, y);
	}
	sort(f+1, f+n+1);
	int pre = f[1], cnt=1, maxx=1, p=1;
	for(int i=2; i<=n; i++)
	{
		//printf(" %d@%d ", i, f[i]);
		if(pre != f[i])
		{
			cnt++;
		  p = 1;
		}
		else
		{
			p++;
			maxx = max(p, maxx);
		}
		pre = f[i];
	}

	cout << cnt << " " << maxx;
	return 0;
}
