//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, m, ans = -999991234, f[CAP_N][CAP_N], dat[CAP_N][CAP_N];

int fun1() {
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
         for (int k = 0; k <= j; k++) {
            if (f[i - 1][k] + dat[i][j - k] > f[i][j]) {
               f[i][j] = f[i - 1][k] + dat[i][j - k];
               if (f[i][j] > ans) ans = f[i][j];
            }
         }
      }
   }
}

void get(int n, int m, int x) {
   if (0 == n) return;
   for (int k = 0; k <= m; k++) {
      if (x == f[n - 1][k] + dat[n][m - k]) {
         get(n - 1, k, f[n - 1][k]);
         printf("%d %d\n", n, m - k);
         break;
      }
   }
}

void work1() {
   scanf("%d%d", &n, &m);
   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= m; j++)
         scanf("%d", &dat[i][j]);
   fun1();
   printf("%d\n", ans);
   get(n, m, ans);
}

int main() {
   work1();
   return 0;
}

/*
3 3 
30 40 50
20 30 50
20 25 30

*/
