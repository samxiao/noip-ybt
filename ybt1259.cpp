#include <bits/stdc++.h>

int a[300], f[300], out[300];
//samxiao 191229
int tail = 1;

int main()
{
   int n, ans = 1, pos = 0, cnt = 0;
   scanf("%d", &n);
   for (int i = 1; i <= n; i++)
   {
      scanf("%d", &a[i]);
   }
   for (int i = 1; i <= n; i++)
   {
      f[i] = 1;
      pos = -1;
      for (int j = 1; j < i; j++)
      {
         if (a[j] <= a[i] && f[j] + 1 > f[i])
         {
            f[i] = f[j] + 1;
            // printf("%d %d/ ", a[j], a[i]);
         }
         if (ans < f[i])
         {
            pos = j;
            ans = f[i];
            // printf(" >%d %d @%d\n", a[j], a[i], ans);
         }
      }
      if (pos > 0)
         out[cnt++] = a[pos];
   }

   printf("max=%d\n", ans);

   int pre = -1;
   for (int i = n; i > 0 && ans; i--)
   {
      //printf(" %d.", f[i]);
      if (f[i] == ans)
      {
         out[tail++] = a[i];
         pre = ans;
         ans--;
      }
   }

   for (int i = tail - 1; i > 0; i--)
      printf("%d ", out[i]);

   return 0;
}

/*

14
13 7 9 16 38 24 37 18 44 19 21 22 63 15

max=8
7 9 16 18 19 21 22 63
=====================================================
19
13 7 9 1 16 3 38 5 33 12 24 37 18 44 19 21 22 63 15

max=9
1 3 5 12 18 19 21 22 63
*/