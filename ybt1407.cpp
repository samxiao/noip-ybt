#include <bits/stdc++.h>

using namespace std;

#define int64_t signed long long
#define uint64_t unsigned long long

int cnt[256];

bool isprime(int n) {
   if (n <= 1) return false;
   for (int i = 2; i * i <= n; i++) {
      if (n % i == 0) return false;
   }
   return true;
}

// V1: AC
void work_v1() {
   char ch = getchar();
   while (ch >= 'a' && ch <= 'z') {
      cnt[ch - 'a']++;
      ch = getchar();
   }

   int maxn = -1, minn = 999999;
   for (int i = 0; i < 26; i++) {
      if (cnt[i]) {
         maxn = max(cnt[i], maxn);
         minn = min(cnt[i], minn);
      }
   }
   if (isprime(maxn - minn))
      printf("Lucky Word\n%d", maxn - minn);
   else
      printf("No Answer\n0");
}

inline bool is_small_alpha(char x) { return 'a' <= x && 'z' >= x; }

/*
v2 8OK, 2WA.
算法有问题，下列样例不能通过。
iiieee
aaaabbbb
最小长度和最大长度是相同的，贴出来的代码最小长度会变成1
*/
void work_v2() {
   char ch[2000];
   memset(ch, 0, sizeof(ch));
   scanf("%s", ch);

   int maxn = -1, minn = 999999;
   for (int i = 0; i < strlen(ch); i++) {
      int t = 0;
      for (int j = i; j < strlen(ch); j++) {
         if (ch[i] == ch[j]) t++;
      }
      maxn = max(t, maxn);
      minn = min(t, minn);
   }
   if (isprime(maxn - minn))
      printf("Lucky Word\n%d", maxn - minn);
   else
      printf("No Answer\n0");
}

void work_v22() {
   char ch[110];
   gets(ch);
   int n = strlen(ch), max = 0, min = 100;
   for (int i = 0; i < n; i++) {
      int t = 0;
      for (int j = 0; j < n; j++)
         if (ch[i] == ch[j]) t++;

      if (t > max) max = t;
      if (t < min) min = t;
   }
   int x = max - min;
   if (x >= 2) {
      int flag = 0;
      for (int i = 2; i * i <= x; i++) {
         if (x % i == 0) {flag = 1;break;}
      }
      if (flag == 0)
         cout << "Lucky Word" << endl << x;
      else cout << "No Answer" << endl << '0';

   } else cout << "No Answer" << endl << "0";
}
//V3: AC
void work_v3() {
   char ch[2000];
   bool flag[200];
   memset(ch, 0, sizeof(ch));
   memset(flag, 0, sizeof(flag));
   scanf("%s", ch);

   int maxn = -1, minn = 999999;
   for (int i = 0; i < strlen(ch); i++) {
      if (flag[ch[i]]) continue;
      int t = 0;
      for (int j = i; j < strlen(ch); j++) {
         if (ch[i] == ch[j]) t++;
      }
      flag[ch[i]] = true;
      maxn = max(t, maxn);
      minn = min(t, minn);
   }
   if (isprime(maxn - minn))
      printf("Lucky Word\n%d", maxn - minn);
   else
      printf("No Answer\n0");
}

int main() {
   work_v22();
   return 0;
}
