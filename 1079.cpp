#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

long long f[100]={1,1,2,3};

void w1()
{
	int m, n, x,y;
	scanf("%d", &n);
	double ans = 0;
 	for(int i=1; i<=n; i++){
		if(i % 2 == 0) ans -= 1.0/i;
		else           ans += 1.0/i;
 	}
	printf("%.4lf", ans);
}

int main()
{
	w1();
  return 0;
}
