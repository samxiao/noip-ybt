#include <bits/stdc++.h>

#define MAXN 100000 + 5
int a[MAXN], f[MAXN], out[MAXN], h[MAXN];
//samxiao 191229
int tail = 1;

int main()
{
   int x, n = 0, ans = 1, pos = 0, cnt = 0;

   while (scanf("%d", &x) != EOF)
      a[++n] = x;

   for (int i = 1; i <= n; i++)
   {
      f[i] = 1;
      for (int j = 1; j < i; j++)
      {
         if (a[j] >= a[i] && f[j] + 1 > f[i])
         {
            f[i] = f[j] + 1;
            // printf("%d %d/ ", a[j], a[i]);
         }
         if (ans < f[i])
         {
            ans = f[i];
            // printf(" >%d %d @%d\n", a[j], a[i], ans);
         }
      }
   }

   for (int i = 1; i <= n; i++)
   {
      if (a[i] < 0)
         continue;
      int hmin = a[i];
      cnt++;
      for (int j = i + 1; j <= n; j++)
      {
         if (a[j] > 0)
         {
            if (a[j] <= hmin)
            {
               hmin = a[j], a[j] = -1;
            }
         }
      }
      a[i] = -1;
   }

   printf("%d\n%d\n", ans, cnt);
   // for (int i = 1; i <= cnt; i++) printf("%d ", h[i]);
   return 0;
}

/*

389 207 155 300 299 170 158 65

6
2
=====================================================
 389 207 155 300 299 170 158 65  189 190 1000 99 1
 389 207 155                 65                  1
            300  299 170 158                  99
 389 207 155 300 299 170 158 65  189 190 1000 99 1
             300 299 170 158 65               99 1

*/