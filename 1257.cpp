//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define XY_N 303

struct xy_t {
   int x, y;
   xy_t() {}
   xy_t(int x, int y) : x(x), y(y) {}
};
int n, a[XY_N][XY_N], ans[XY_N][XY_N];
queue<xy_t> q;

void init() {
   memset(a, 0, sizeof(a));
   memset(ans, 0, sizeof(ans));
}

inline bool isIn(xy_t p) { return 0 <= p.x && p.x < n && 0 <= p.y && p.y < n; }
void bfs(xy_t fr) {
   if (!isIn(fr) || a[fr.x][fr.y]) return;
   q.push(fr);
   a[fr.x][fr.y] = 1;
   while (!q.empty()) {
      xy_t p = q.front();
      q.pop();
      int dxy[][2] = {{1, 2}, {1, -2}, {-1, 2}, {-1, -2}, {2, 1}, {2, -1}, {-2, 1}, {-2, -1}};
      for (int i = 0; i < szARR_CNT_OF(dxy); i++) {
         int nx = p.x + dxy[i][0], ny = p.y + dxy[i][1];
         if (isIn(xy_t(nx, ny)) && !a[nx][ny]) {
            a[nx][ny] = 1;
            ans[nx][ny] = ans[p.x][p.y] + 1;
            q.push(xy_t(nx, ny));
         }
      }
   }
}

void work1() {
   int t;
   scanf("%d", &t);
   while (t--) {
      init();
      xy_t xfr, xto;
      scanf("%d", &n);
      scanf("%d%d", &xfr.x, &xfr.y);
      scanf("%d%d", &xto.x, &xto.y);

      bfs(xfr);
      printf("%d\n", ans[xto.x][xto.y]);
   }
}

int main() {
   work1();
   return 0;
}