#include <bits/stdc++.h>
using namespace std;
 
#define U64_t unsigned long long

U64_t num[4*1000*1000];
U64_t blah(U64_t a1, U64_t n)
{
   U64_t cnt = 0, top = 0;
   U64_t two = 0, three = 0;
   num[top++] = a1;
   while (cnt++ < n)
   {
      U64_t t2, t3, t;
      t2 = 2 * num[two] + 1;
      t3 = 3 * num[three] + 1;
      t = min(t2, t3);
      num[top++] = t;
      if (t == t2) two++;
      if (t == t3) three++;
      //if (t != num[top - 1]) num[top++] = t;
      //if(t2==t3) printf(" %lld@%lld.%lld/%lld " , t, cnt, two, three);
   }
   return  num[n - 1];
}
 
int main()
{
   U64_t a1, n;
   while(scanf("%lld%lld", &a1, &n) != EOF){
      printf("%lld\n", blah(a1, n));
   }
   return 0;
}