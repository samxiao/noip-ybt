#include <bits/stdc++.h>
using namespace std;

int a[100086];
int f[100086];

int main() {
   int k, n, t, ans;
   scanf("%d", &t);
   while (t--) {
      scanf("%d", &n);
      ans = 0;
      memset(f, 0, sizeof(f));
      for (int i = 1; i <= n; i++) scanf("%d", a + i);

      f[1] = a[1];
      for (int i = 2; i <= n; i++) {
         f[i] = max(f[i - 1], f[i - 2] + a[i]);
      }
      printf("%d\n", f[n]);
   }
}