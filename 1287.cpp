//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, m, ans = -999991234, f[CAP_N][CAP_N], dat[CAP_N][CAP_N];

int fun1(int n) {
   int res = 0;

   memset(f, 0, sizeof(f));
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
         int dxy[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
         for (int q1 = 0; q1 < 4; q1++) {
            int nx = i + dxy[q1][0], ny = j + dxy[q1][1];
            if (f[nx][ny])
               f[i][j] = f[i][j] ? min(f[i][j], f[nx][ny]) : f[nx][ny];
         }
         f[i][j] += dat[i][j];
      }
   }

   printf("%d", f[n][n]);
}

void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= n; j++)
         scanf("%d", &dat[i][j]);
   fun1(n);
}

int main() {
   work1();
   return 0;
}
