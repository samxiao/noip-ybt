//SamXIAO
#include <bits/stdc++.h>
using namespace std;


bool isprime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}
int f[10000], cnt;
void gen()
{
	for(int i = 3; i < 100002; i += 2)
		if(isprime(i)) f[cnt++] = i;
}

bool isRev(int x)
{
	int a[10]={0}, cnt = 0;
	while(x){
		a[cnt++] = x % 10;
		x /= 10;
	}
	for(int i = 0, j = cnt - 1; i <= j; i++, j--){
		if(a[i] != a[j]) return false;
	}
	return true;
}
void work1()
{
	int m, n, pos=1, ans = 0;
	scanf("%d", &n);
	gen();
	for(int i = 1; i < cnt; i++){
		if(11 <= f[i] && f[i] <= n && isRev(f[i])) ans++;
	}
	printf("%d", ans);
}

int main()
{
	work1();
  return 0;
}
