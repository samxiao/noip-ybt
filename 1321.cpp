#include <bits/stdc++.h>
using namespace std;

char a[1000];
void del1() {
   int len = strlen(a), pos = 1;
   while (a[pos - 1] <= a[pos] && pos < len) {
      pos++;
   }

   for (int i = pos - 1; i < len; i++) a[i] = a[i + 1];
}
void work1() {
   int k;
   scanf("%s%d", a, &k);
   if (k >= strlen(a)) {
      printf("0");
      return;
   }
   while (k--) {
      del1();
   }
   k = 0;
   while ('0' == a[k]) k++;
   printf("%s", &a[k]);
}

int main() {
   work1();
   return 0;
}