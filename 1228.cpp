#include <bits/stdc++.h>
using namespace std;

int a[20086];
bool cmp(int a, int b) { return a > b; }

void work1() {
   int n, b;
   scanf("%d%d", &n, &b);
   for (int i = 0; i < n; i++) scanf("%d", a + i);
   sort(a, a + n, cmp);
   int ans = 0;
   while (b > 0) {
      b -= a[ans++];
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
