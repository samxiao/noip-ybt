#include <bits/stdc++.h>
using namespace std;

int a[20086];
bool cmp(int a, int b) { return a > b; }

//OJ没有这类测试数据： 3 3 5 10000
void work1() {
   int n, b;
   while (scanf("%d", &n) != EOF) {
      int ans = 0;
      for (int i = 0; i < n; i++) {
         int x;
         scanf("%d", &x);
         a[i] = x;
         ans += x;
      }
      if (2 == n)
         printf("%d\n", min(a[0], a[1]));
      else
         printf("%.1f\n", ans * 1.0 / 2);
   }
}

int main() {
   work1();
   return 0;
}
