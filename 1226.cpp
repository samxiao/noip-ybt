#include <bits/stdc++.h>
using namespace std;

int a[10086], p[10086];
void work1() {
   int k, n;

   while (true) {
      int cnt0 = 0, ans = 0;
      for (int i = 1; i <= 6; i++) {
         int x;
         scanf("%d", &x);
         a[i] = x;
         if (0 == x) cnt0++;
      }
      ans += a[6] + a[5] + a[4] + (a[3] % 4 ? a[3] / 4 + 1 : a[3] / 4);
      a[3] %= 4;
      if (1 == a[3]) p[2] = 5, p[1] = 7;
      if (2 == a[3]) p[2] = 3, p[1] = 6;
      if (3 == a[3]) p[2] = 1, p[1] = 5;

      p[2] += a[4] * 5;
      p[1] += a[5] * 11;

      a[1] -= 11 * p[5];
      if (p[2] < a[2]) {
         a[2] -= p[2];
         p[2] = 0;
      } else if (p[2] == a[2]) {
         p[2] = 0;
         a[2] = 0;
      } else {
         p[2] -= a[2];
         a[2] = 0;
      }
      if (a[2]) ans += a[2] % 9 ? a[2] / 9 + 1 : a[2] / 9;
      if (!(a[2] % 9)) {
         p[1] += (9 - a[2] % 9) * 4;
      }
      if (p[1] < a[1]) {
         a[1] -= p[1];
         ans += a[1] / 36;
         if (a[1] % 36) ans++;
      }

      if (6 == cnt0) break;

      printf("%d\n", ans);
   }
}

int main() {
   work1();
   return 0;
}