//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 1086
#define LIMIT_MAX 0x1ffffff
int n, ans, f[CAP_N][CAP_N], cnt;
char a[CAP_N], b[CAP_N];

void work1() {
   scanf("%s%s", a, b);
   for (int i = 1; i <= strlen(a); i++) {
      for (int j = 1; j <= strlen(b); j++) {
         f[i][j] = max(f[i - 1][j], f[i][j - 1]);
         if (a[i - 1] == b[j - 1]) {
            f[i][j] = max(f[i][j], f[i - 1][j - 1] + 1);
         }
         ans = max(ans, f[i][j]);
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}