#include <bits/stdc++.h>
using namespace std;

struct ele_t {
   int s, e;
};
ele_t a[10086];

bool cmp(ele_t x1, ele_t x2) { return x1.e < x2.e; }

void work1() {
   int x, n = 0;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) scanf("%d%d", &a[i].s, &a[i].e);

   sort(a, a + n, cmp);

   int ans = 1;
   ele_t base = a[0];
   for (int i = 1; i < n; i++) {
      if (a[i].s > base.e) {
         ans++;
         base = a[i];
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}