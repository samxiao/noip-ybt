#include <bits/stdc++.h>

long long f[5000];
int a[300];

int main() {
   int t, n;
   scanf("%d%d", &n, &t);
   for (int i = 1; i <= n; i++) scanf("%d", a + i);
   f[0] = 1;
   for (int i = 1; i <= n; i++)
      for (int j = t; j >= a[i]; j--)
         f[j] += f[j - a[i]];

   printf("%lld", f[t]);
   return 0;
}