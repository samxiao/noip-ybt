#include <bits/stdc++.h>
using namespace std;

long long ans = 0;
template <class _T>
void msort(_T *dat, int l, int r) {
   if (l >= r) return;
   int mid = (l + r) / 2;
   msort(dat, l, mid);
   msort(dat, mid + 1, r);
   int i = l, j = mid + 1, k = 0;
   _T *tmp = new _T[r - l + 1];
   while (i <= mid && j <= r) {
      if (dat[i] > dat[j]) {
         tmp[k++] = dat[i++];
      } else {
         ans += mid - i + 1;
         tmp[k++] = dat[j++];
      }
   }
   while (i <= mid) tmp[k++] = dat[i++];
   while (j <= r) tmp[k++] = dat[j++];
   for (int i = l; i <= r; i++) dat[i] = tmp[i - l];
   delete tmp;
}

long long dat[100086];
int main() {
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) scanf("%lld", &dat[i]);
   msort<long long>(dat, 0, n - 1);
   for (int i = 0; i < n; i++) printf("%lld ", dat[i]);
   printf("%lld", ans);

   return 0;
}
/*
noilinux@noi-VM:~/Desktop/C$ g++ msort.cpp
msort.cpp: In function ‘int main()’:
msort.cpp:31:52: warning: format ‘%ld’ expects argument of type ‘long int*’, but argument 2 has type ‘long long int*’ [-Wformat=]
    for (int i = 0; i < n; i++) scanf("%ld", &dat[i]);
                                                    ^
msort.cpp:33:21: warning: format ‘%ld’ expects argument of type ‘long int’, but argument 2 has type ‘long long int’ [-Wformat=]
    printf("%ld", ans);
                     ^
noilinux@noi-VM:~/Desktop/C$ ./a.out 
5
5 4 3 2 1
*** Error in `./a.out': free(): invalid next size (fast): 0x083a9008 ***
已放弃
noilinux@noi-VM:~/Desktop/C$ 
*/