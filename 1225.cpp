#include <bits/stdc++.h>
using namespace std;

struct ele_t {
   int w, v;
   double p;
};

ele_t a[10086];
bool cmp(ele_t x1, ele_t x2) { return x1.p > x2.p; }
void work1() {
   int k, n;
   scanf("%d", &k);
   while (k--) {
      double w;
      scanf("%lf%d", &w, &n);
      memset(a, 0, sizeof(a));
      for (int i = 1; i <= n; i++) {
         scanf("%d%d", &a[i].w, &a[i].v);
         a[i].p = a[i].v * 1.0 / a[i].w;
      }
      sort(a + 1, a + 1 + n, cmp);
      double ans = 0;
      int pos = 1;
      while (w > 0) {
         if (w >= a[pos].w) {
            ans += a[pos].v;
            w -= a[pos++].w;
         } else {
            ans += w / a[pos].w * a[pos].v;
            break;
         }
      }
      printf("%.2lf\n", ans);
   }
}

int main() {
   work1();
   return 0;
}