//Sam.XIAO
#include <bits/stdc++.h>
using namespace std;

int a[100085];
inline double Cal(double x) {
   double res = x * x * x * x * x;
   res += -15 * x * x * x * x;
   res += 85 * x * x * x;
   res += -225 * x * x + 274 * x - 121;
   return res;
}

void work1() {
   int l = 0, r = 1000 * 1000 * 10, mid;
   double step = 1e-7, x, y;
   while (l < r) {
      mid = l + (r - l) / 2;
      x = step * mid;
      y = Cal(1.5 + x);
      if (0 == y) {
         printf("%.6lf", 1.5 + x);
         return;
      } else if (0 < y) {
         l = mid + 1;
      } else {
         r = mid - 1;
      }
   }
   x = step * mid + 1.5;
   printf("%.6lf", x);
   //printf("  %lf", Cal(x));
}

int main() {
   work1();
   return 0;
}
