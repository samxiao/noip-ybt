#include <iostream>
#include <bits/stdc++.h>
using namespace std;

#define MAXN 300
int f[MAXN], q[30004], flag[MAXN], tail, head;
int main()
{
   int a, b, n, k, ans = 0;
   cin >> n >> a >> b;

   for (int i = 1; i <= n; i++) cin >> f[i];

   q[tail++] = a;
   flag[a] = 1;
   while ( head != tail)
   {
      int fCur = q[head++];
      if(fCur == b ){
         printf("%d", flag[fCur] - 1);
         return 0;
      }
      int fNxt = fCur - f[fCur];
      if(fNxt >  0 && !flag[fNxt]) q[tail++] = fNxt, flag[fNxt] = flag[fCur] + 1;
      fNxt = fCur + f[fCur];
      if(fNxt <= n && !flag[fNxt]) q[tail++] = fNxt, flag[fNxt] = flag[fCur] + 1;
   }
   printf("-1");
   return 0;
}
