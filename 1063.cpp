#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int n, x, ma=-999999, mi=999999;
	scanf("%d",  &n);
	for(int i=0; i<n; i++){
		scanf("%d", &x);
		if(ma < x) ma = x;
		if(mi > x) mi = x;
	}
	printf("%d", ma-mi);
  return 0;
}
