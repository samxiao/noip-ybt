//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAXLEN 5000

long long baseN;

class HPNum {
  private:
   struct num_t {
      long long x[MAXLEN];
      int len;
   };
   int remain = 0;
   num_t nums;
   void clear() {
      memset(nums.x, 0, sizeof(nums.x));
   }

  public:
   HPNum(char *s) {
      clear();
      for (int i = 0; i < strlen(s); i++) nums.x[i] = s[i] - '0';
      nums.len = strlen(s);
   }
   HPNum() { clear(); }
   HPNum(num_t *nptr) {
      for (int i = 0; i < nptr->len; i++) nums.x[i] = nptr->x[i];
      nums.len = nptr->len;
   }
   HPNum operator/(const int x2) {
      HPNum x3(&nums);
      int x1 = 0;
      for (int i = 0; i < x3.nums.len; i++) {
         x1 += x3.nums.x[i];
         x3.nums.x[i] = x1 / x2;
         x1 %= x2;
         x1 *= 10;
      }
      remain = x1/10;
      x3.remain = remain;
      return x3;
   }

   void print() {
      int pos = 0;
      while (0 == nums.x[pos] && pos < nums.len) pos++;
      while (pos < nums.len) {
         printf("%d", nums.x[pos++]);
      }
      printf("\n%d", remain);
   }
   ~HPNum() {}
};

void work1() {
   char s1[MAXLEN] = {0}, s2[MAXLEN] = {0};
   int n;
   memset(s1, 0, sizeof(s1));
   memset(s2, 0, sizeof(s2));
   scanf("%s", s1);

   HPNum a(s1);
   (a / 13).print();
}

int main() {
   work1();
   return 0;
}
