//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 5500  // 10 * 1000 * 1000 + 86

float dat[MAX_LEN], dat1[MAX_LEN];
bool cmp(float x1, float x2) { return x1 < x2; }
bool cmp1(float x1, float x2) { return x1 > x2; }
void work1() {
   int n, cnt0 = 0, cnt1 = 0;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      float x;
      char fm[10];
      scanf("%s%f", fm, &x);
      if ('f' == fm[0])
         dat1[cnt1++] = x;
      else
         dat[cnt0++] = x;
   }

   sort(dat, dat + cnt0, cmp);
   sort(dat1, dat1 + cnt1, cmp1);

   for (int i = 0; i < cnt0; i++) {
      printf("%.2f ", dat[i]);
   }

   for (int i = 0; i < cnt1; i++) {
      printf("%.2f ", dat1[i]);
   }

}

int main() {
   work1();
   return 0;
}
