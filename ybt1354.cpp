#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long
char stk[10000];
int top;
 

bool judge(char c){
   if(top<=0) return false;
   if (stk[--top] != c) return  false;       
   return true;
}

int main()
{
   string s;
   char ch;
   cin >> s;
   bool isOk = true;
   for(int i=0; i<s.length(); i++) 
   {
      ch = s[i];
      if ('[' == ch || '(' == ch) stk[top++] = ch;
      if (')' == ch) 
      {
         isOk = judge('(');
         if(!isOk) break;
      }
      if (']' == ch)
      {
         isOk = judge('[');
         if (!isOk) break;
      }
   }

   printf("%s", isOk && top == 0 ? "OK" : "Wrong");
   return 0;
}