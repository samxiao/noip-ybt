//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int a[10008], step;

void getans(int n, int remain) {
   if (0 == remain) return;
   int nTail = n - remain + 1;

   const char *stp4[] = {
       "oooo****--",
       "ooo--***o*",
       "ooo*o**--*",
       "o--*o**oo*",
       "o*o*o*--o*",
       "--o*o*o*o*"};

   if (4 >= remain) {
      for (int i = 0; i < 6; i++) {
         printf("step%2d:", step++);
         printf("%s", stp4[i]);
         for (int i = 0; i < nTail - 1; i++) printf("o*");
         printf("\n");
      }
      return;
   }
   printf("step%2d:", step++);
   for (int i = 0; i < remain; i++) printf("o");
   for (int i = 0; i < remain; i++) printf("*");
   printf("--");
   for (int i = 0; i < nTail - 1; i++) printf("o*");
   printf("\n");

   printf("step%2d:", step++);
   for (int i = 0; i < remain - 1; i++) printf("o");
   printf("--");
   for (int i = 0; i < remain - 1; i++) printf("*");
   for (int i = 0; i < nTail; i++) printf("o*");
   printf("\n");

   getans(n, remain - 1);
}

void work1() {
   int n;
   scanf("%d", &n);
   getans(n, n);
}

int main() {
   work1();
   return 0;
}
