#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

long long f[100]={1,1,2,3};

void w1()
{
	for(int i=2; i<50; i++) f[i] = f[i-1] + f[i-2];
	int m, n, x,y;
	scanf("%d", &n);
	double ans = 0;
 	for(int i=1; i<=n; i++){
		ans += f[i+1]/(double)(f[i]);
 	}
	printf("%.4lf", ans);
}

int main()
{
	w1();
  return 0;
}
