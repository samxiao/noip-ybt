//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

int a[1000085];
long long ans;

// luogu p1190. 注意m=1
void work1() {
   int n, m;
   scanf("%d", &n);

   for (int i = 0, x; i < n; i++) {
      scanf("%d", &x);
      a[i] = x;
   }

   sort(a, a + n);
   for (int i = 0; i < n; i++) {
      if (0 == i || a[i] != a[i - 1]) printf("%d ", a[i]);
   }
}

int main() {
   //freopen("P1190_10.in", "r", stdin);
   work1();
   return 0;
}