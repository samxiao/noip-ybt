#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int n;
	double x, ans=0;
	scanf("%d",  &n );
	for(int i=0; i<n; i++){
		scanf("%lf", &x);
		ans += x;
	}
	printf("%.4lf", ans / n);
  return 0;
}
