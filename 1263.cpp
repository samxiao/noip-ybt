//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 5005
#define LIMIT_MAX 0x1ffffff
int n, ans, f[CAP_N];

struct city_t {
   int l, r;
} dat[CAP_N];

bool cmp(city_t a, city_t b) {
   if (a.l == b.l) return a.r < b.r;
   return a.l < b.l;
}

void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      scanf("%d%d", &dat[i].l, &dat[i].r);
   }
   sort(dat + 1, dat + n + 1, cmp);
   for (int i = 1; i <= n; i++) DBG_PRINT("%d,%d  %s\n", dat[i].l, dat[i].r, i == n ? "\n" : " ");
   for (int i = 1; i <= n; i++) {
      f[i] = 1;
      for (int j = 1; j < i; j++) {
         if (dat[i].r > dat[j].r) f[i] = max(f[i], f[j] + 1);
      }
      ans = max(ans, f[i]);
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
