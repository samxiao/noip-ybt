/SamXIAO
#include <bits/stdc++.h>
using namespace std;

double f(int n,  double x)
{
	if(1 == n) return sqrt(1 + x);
	return sqrt(n+f(n-1, x));
}

void work1()
{
	int n;
	double x;
	scanf("%lf%d", &x, &n);
	printf("%.2lf", f(n ,x));
}

int main()
{
	work1();
  return 0;
}
