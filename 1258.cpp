#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT(x) (sizeof(x) / sizeof(x[0]))
#define MAXN 1087
long long a[MAXN][MAXN];
long long num, ans, n, m;

void work1() {
   scanf("%d", &n);
   int x, y;
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= i; j++) {
         scanf("%lld", &a[i][j]);
      }
   }
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= i; j++) {
         a[i][j] += max(a[i - 1][j], a[i - 1][j - 1]);
      }
   }

   for (int j = 1; j <= n; j++) {
      ans = max(a[n][j], ans);
   }
   printf("%lld", ans);
}

int main() {
   work1();
   return 0;
}
