#include <bits/stdc++.h>
using namespace std;

int dis[3800], c[3800];
int f[20086];

int main() {
   int k, n, t, ans;
   scanf("%d", &t);
   while (t--) {
      scanf("%d%d", &n, &k);
      ans = 0;
      memset(dis, 0, sizeof(dis));
      memset(f, 0, sizeof(f));
      for (int i = 1; i <= n; i++) scanf("%d", dis + i);
      for (int i = 1; i <= n; i++) scanf("%d", c + i);

      for (int i = 1; i <= n; i++) {
         f[i] = c[i];
         for (int j = 1; j < i; j++)
            if (dis[i] - dis[j] > k) f[i] = max(f[i], f[j] + c[i]);

         ans = max(ans, f[i]);
      }
      printf("%d\n", ans);
   }
}