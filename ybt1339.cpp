#include <bits/stdc++.h>
using namespace std;

#if 0 //#ifndef ONLINE_JUDGE
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define MAXX
/* 1339：【例3-4】求后序遍历
SamXIAO 200125 
*/

string FirstMiddleToLast(string fir, string mid)
{
   string sRet = "";
   if (fir.length() <= 0) return "";
   if (2 == fir.length()) return fir.substr(1, 1) + fir.substr(0, 1);
   if (1 == fir.length()) return fir;
   int j = mid.find(fir[0]);
   string midL, midR, firL, firR;
   midL = mid.substr(0, j);
   midR = mid.substr(j + 1, mid.length() - j - 1);
   if (j == string::npos) return "";
   if (    j == 0               ) firL = "", firR = fir.substr(1, fir.length() - 1);
   else if(j == mid.length() - 1) firR = "", firL = fir.substr(1, fir.length() - 1);
   else
   {
      if (j == mid.length() - 2) j = fir.find(mid[j + 1]);
      else 
      {
         int posmin = 0x8000000;
         while(j < mid.length()-1) posmin = min((int)fir.find(mid[++j]), posmin);
         j = posmin;
      }
      firL = j == 0 ? "" : fir.substr(1, j - 1);
      firR = fir.substr(j, fir.length() - j);
   }
   sRet = FirstMiddleToLast(firR, midR) + fir.substr(0, 1);
   sRet = FirstMiddleToLast(firL, midL) + sRet;

   return sRet;
}

int main()
{
   string fir, mid;
   cin >> fir >> mid;
   cout << FirstMiddleToLast(fir, mid);
   return 0;
}

/*
ABDFEGCHIK
FDBEGAHCIK

FDGEBHKICA
==================
abdec
dbeac

debca
*/
