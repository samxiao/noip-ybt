//SamXIAO
#include <bits/stdc++.h>
using namespace std;

double getmax(double x, double y, double z)
{
	if(x < y) x = y;
	if(x < z) x = z;
	return x;
}
void w1()
{
	double a, b, c;
	double ans = 0;
	scanf("%lf%lf%lf", &a, &b, &c);
	ans = getmax(a, b, c)/(getmax(a + b, b, c) * getmax(a, b, b + c));
	printf("%.3lf", ans);
}

int main()
{
	w1();
  return 0;
}
