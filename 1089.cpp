#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

long long f[100]={1,1,2,3};

void w1()
{
	int a,b,n, k;
	scanf("%d", &k);
	n=0;
	bool isNeg = k < 0;
	if(isNeg) k = -k;
	for(;k>0;){
		n *= 10;
		n += k % 10;
		k /= 10;
	}
	printf("%d", n * (isNeg ? -1 : 1));
}

int main()
{
	w1();
  return 0;
}
