//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N 10000
// 1168：大整数加法
class HPNum {
  private:
   struct num_t {
      int x[10086];
      int len;
   };
   num_t nums;

   static void str2num(char *s, num_t *num) {
      int pos = 0;
      while (s[pos] == '0' && pos < strlen(s)) pos++;
      num->len = strlen(s) - pos;
      for (int i = strlen(s) - 1, j = 0; i >= pos; i--, j++)
         num->x[j] = s[i] - '0';
   }

  public:
   HPNum(char *a1) {
      clear();
      str2num(a1, &nums);
   }

   HPNum(num_t b) {
      clear();
      nums.len = b.len;
      for (int i = 0; i < b.len; i++)
         nums.x[i] = b.x[i];
   }

   void clear() {
      memset(nums.x, 0, sizeof(nums.x));
   }
   HPNum() {
      clear();
   }

   void operator=(const HPNum b) {
      clear();
      nums.len = b.nums.len;
      for (int i = 0; i < b.nums.len; i++)
         nums.x[i] = b.nums.x[i];
   }

   HPNum operator+(const HPNum b) {
      HPNum x1(nums), x3;

      num_t *c = &x3.nums;
      int lc = c->len = max(b.nums.len, x1.nums.len);

      for (int i = 0; i < lc; i++) {
         x3.nums.x[i] += b.nums.x[i] + x1.nums.x[i];
         if (x3.nums.x[i] >= 10) x3.nums.x[i + 1]++, x3.nums.x[i] -= 10;
      }
      if (x3.nums.x[lc]) c->len++;

      return x3;
   }

   void print() {
      for (int i = nums.len - 1; i >= 0; i--)
         printf("%d", nums.x[i]);
   }

   ~HPNum() {}
};

void work1() {
   char s1[10086], s2[10086];
   scanf("%s%s", s1, s2);
   HPNum a(s1), b(s2), c;
   (a + b).print();
}

int main() {
   work1();
   return 0;
}
