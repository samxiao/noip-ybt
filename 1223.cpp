#include <bits/stdc++.h>
using namespace std;

char a[100];

int getN(int x) {
   int x2 = x, pos = 0, pos1 = 0, pos0 = 0;
   while (x2) {
      a[pos++] = x2 % 2;
      if (!pos1 && x2 % 2) pos1 = pos;
      if (!pos0 && pos1 && 0 == x2 % 2) pos0 = pos;
      x2 /= 2;
   }
   if (0 == pos0) pos0 = pos + 1, pos++;
   pos1--;
   pos0--;

   a[pos0] = 1;
   a[pos0 - 1] = 0;
   int theP = pos0 - pos1 - 1;
   //printf(" %d.%d %d, =%d \n", pos, pos1, pos0, theP);
   if (theP > 0) {
      for (int i = 0; i < theP; i++) a[pos1 + i] = 0, a[i] = 1;
   }

   int r = 0;
   while (pos--) {
      r *= 2;
      r += a[pos];
   }
   return r;
}

void work1() {
   int x = 1;
   while (true) {
      scanf("%d", &x);
      if (0 == x) break;
      printf("%d\n", getN(x));
   }
}

int main() {
   work1();
   return 0;
}