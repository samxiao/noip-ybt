//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}
#define N_MAX 987654321
#define N_MIN -N_MAX
int f[286];
char s1[1086];
char s2[2020];
/*读题不全若第一个串s1是第二个串s2的子串，则输出(s1) is substring of (s2)
否则，若第二个串s2是第一个串s1的子串，输出(s2) is substring of (s1)
*/
void str2arr(string s, char *p)
{
	for(int i=0; i<s.length(); i++) p[i] = s.c_str()[i];
}
void w1()
{
	int cnt = 0, ans = 0, n;
	//scanf("%d", &n);
	string st1, st2;
	cin >> st1 >> st2;
	bool isSub = false;
	int l1 = st1.length(), l2 = st2.length();
	if(l1 > l2){
		swap(l1, l2);
		str2arr(st1, s2);
		str2arr(st2, s1);
	}
	else{
		str2arr(st1, s1);
		str2arr(st2, s2);
	}
	
	int pos = 0;
	while(pos + l1 <= l2){
		isSub = true;
		for(int i=0; i<strlen(s1); i++){
			if(s2[pos + i] != s1[i]){
				isSub = false;
				break;
			}
		}
		if(isSub) break;
		pos++;
	}
	if(isSub) printf("%s is substring of %s", s1, s2);
	else printf("No substring");
	
}

int main()
{
	w1();
  return 0;
}
