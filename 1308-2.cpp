//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N 10000
// 1308������1.5���߾���

//�߾��� High precision divsion

class HPNum {
  private:
   struct num_t {
      int x[10086];
      int len;
   };
   num_t nums;

   static void str2num(char *s, num_t *num) {
      num->len = strlen(s);
      for (int i = 0; i < strlen(s); i++)
         num->x[i] = s[i] - '0';
   }

   static int compare(int const *a, int const *b, int len) {
      if (a[-1] > 0) return 1;
      for (int i = 0; i < len; i++) {
         if (a[i] != b[i]) return a[i] > b[i] ? 1 : -1;
      }
      return 0;
   }

   static int getone(int *a, int const *b, int blen) {
      int res = 0;
      while (compare(a, b, blen) >= 0) {
         res++;
         for (int i = blen - 1; i >= 0; i--) {
            a[i] -= b[i];
            if (a[i] < 0) a[i] += 10, a[i - 1]--;
         }
      }
      return res;
   }

   static HPNum hpdiv(const HPNum a, const HPNum b, bool isDivOrMode) {
      HPNum x3, x1(a);

      num_t *c = &x3.nums;
      int pos = 1, lc = 0;
      while (pos + b.nums.len <= a.nums.len) {
         c->x[lc++] = getone(&x1.nums.x[pos], b.nums.x, b.nums.len);
         pos++;
      }
      c->len = lc;
      return isDivOrMode ? x3 : x1;
   }

  public:
   HPNum(char *a1) {
      str2num(a1, &nums);
      for (int i = nums.len - 1; i >= 0; i--)
         nums.x[i + 1] = nums.x[i];
      nums.x[0] = 0;
      nums.len++;
   }

   HPNum(num_t b) {
      nums.len = b.len;
      for (int i = 0; i < b.len; i++)
         nums.x[i] = b.x[i];
   }

   HPNum() {
      memset(nums.x, 0, sizeof(nums.x));
   }

   void operator=(const HPNum b) {
      nums.len = b.nums.len;
      for (int i = 0; i < b.nums.len; i++)
         nums.x[i] = b.nums.x[i];
   }

   HPNum operator/(const HPNum b) {
      HPNum x1(nums);
      return hpdiv(x1, b, true);
   }

   HPNum operator%(const HPNum b) {
      HPNum x1(nums);
      return hpdiv(x1, b, false);
   }

   void print() {
      int pos = 0;
      while (!nums.x[pos] && pos < nums.len - 1) {
         pos++;
      }

      for (int i = pos; i < nums.len; i++)
         printf("%d", nums.x[i]);
   }

   ~HPNum() {}
};

void work1() {
   char s1[10086], s2[10086];
   scanf("%s%s", s1, s2);
   HPNum a(s1), b(s2), c;
   c = a / b;
   c.print();
   printf("\n");
   (a % b).print();
}

int main() {
   work1();
   return 0;
}
