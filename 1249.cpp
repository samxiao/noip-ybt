//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char a[1008][1086];
int ans, n, m;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
// unformat
int dxy[][2] = {
    {0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};

void dfs(int x, int y) {
   if (0 > x || 0 > y || x >= n || y >= m) return;
   if ('W' != a[x][y]) return;
   a[x][y] = '.';
   for (int i = 0; i < szARR_CNT_OF(dxy); i++) {
      int nx = x + dxy[i][0], ny = y + dxy[i][1];
      dfs(nx, ny);
   }
}

void work1() {
   scanf("%d%d", &n, &m);
   for (int i = 0; i < n; i++) scanf("%s", a[i]);
   for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
         if ('W' == a[i][j]) {
            ans++;
            dfs(i, j);
         }
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
