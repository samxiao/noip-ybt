//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif

int a[108][106];
int res[108][108];
struct xy_t {
   int x, y;
   xy_t(){};
   xy_t(int x, int y) : x(x), y(y) {}
};
xy_t prevs[100][100];
queue<xy_t> q;
int ans, n = 5, m = 5, step;

bool isIn(int x, int y) {
   return 0 <= x && 0 <= y && x < n && y < m;
}

void bfs(int x, int y) {
   if (a[x][y]) return;
   a[x][y] = 1;
   step++;
   res[x][y] = step;
   q.push(xy_t(x, y));
   while (!q.empty()) {
      xy_t rm = q.front();
      q.pop();
      x = rm.x, y = rm.y;
      DBG_PRINT("%d,%d  ", x, y);
      int dxy[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
      for (int i = 0; i < szARR_CNT_OF(dxy); i++) {
         int nx, ny;
         nx = x + dxy[i][0], ny = y + dxy[i][1];
         if (isIn(nx, ny) && 0 == a[nx][ny]) {
            a[nx][ny] = 1;
            res[nx][ny] = res[x][y] + 1;
            prevs[nx][ny].x = x, prevs[nx][ny].y = y;
            q.push(xy_t(nx, ny));
            DBG_PRINT("  >>%d,%d  ", nx, ny);
         }
      }
      DBG_PRINT("\n");
   }
   DBG_PRINT("  @%d\n", res[0][0]);
}

void work1() {
   for (int i = 0; i < n; i++)
      for (int j = 0; j < m; j++) scanf("%d", &a[i][j]);

   bfs(4, 4);

   for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) DBG_PRINT("%d,%d   ", prevs[i][j].x, prevs[i][j].y, j == m - 1 ? "\n" : "");
      DBG_PRINT("\n");
   }
   int x2 = 0, y2 = 0;
   xy_t &pos = prevs[x2][y2];
   while (true) {
      printf("(%d, %d)\n", x2, y2);
      x2 = pos.x;
      y2 = pos.y;
      pos = prevs[x2][y2];
      if (x2 == 4 && 4 == y2) break;
   }
   printf("(4, 4)");
}

int main() {
   work1();
   return 0;
}