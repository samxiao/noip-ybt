#include <bits/stdc++.h>
using namespace std;

struct span_t {
   int l, r;
};
span_t dat[500096];
bool cmp(span_t x1, span_t x2) {
   if (x1.l == x2.l) return x1.r < x2.r;
   return x1.l < x2.l;
}
int main() {
   int n, maxR = 0;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      scanf("%d%d", &dat[i].l, &dat[i].r);
      if (maxR < dat[i].r) maxR = dat[i].r;
   }
   sort(dat, dat + n, cmp);
   int L = dat[0].l;
   int R = dat[0].r;
   for (int i = 1; i < n; i++) {
      if (R >= dat[i].l) {
         R = max(R, dat[i].r);
      } else {
      }
   }
   if (R == maxR)
      printf("%d %d", L, R);
   else
      printf("no");
   return 0;
}
