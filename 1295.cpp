#include <bits/stdc++.h>
using namespace std;

int w[3800], c[3800];
int f[20086];

int main() {
   int m, n;
   scanf("%d%d", &m, &n);
   for (int i = 1; i <= n; i++) scanf("%d", w + i);
   for (int i = 1; i <= n; i++)
      for (int j = m; j >= w[i]; j--)
         f[j] = max(f[j], f[j - w[i]] + w[i]);

   printf("%d", m - f[m]);
}