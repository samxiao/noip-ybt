#include <bits/stdc++.h>
using namespace std;

char a[102][102];
//bool vi[102][102];
bool f[255];
int ans;
void combination(int rows, int cols, int x, int y, int stp) {
   if (ans < stp) ans = stp;

   f[a[x][y]] = true;
   int dxy[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
   for (int i = 0; i < 4; i++) {
      int nx = x + dxy[i][0], ny = y + dxy[i][1];
      if (!f[a[nx][ny]] && 0 <= ny && ny < cols && 0 <= nx && nx < rows) {
         f[a[nx][ny]] = true;
         combination(rows, cols, nx, ny, stp + 1);
         f[a[nx][ny]] = false;
      }
   }
}
void srch(int rows, int cols, int x, int y, int stp) {
   if (ans < stp) ans = stp;

   f[a[x][y]] = true;
   int nx, ny;
   nx = x - 1, ny = y;
   if (!f[a[nx][ny]] && 0 <= ny && ny < cols && 0 <= nx && nx < rows) {
      f[a[nx][ny]] = true;
      srch(rows, cols, nx, ny, stp + 1);
      f[a[nx][ny]] = false;
   }

   nx = x + 1, ny = y;
   if (!f[a[nx][ny]] && 0 <= ny && ny < cols && 0 <= nx && nx < rows) {
      f[a[nx][ny]] = true;
      srch(rows, cols, nx, ny, stp + 1);
      f[a[nx][ny]] = false;
   }

   nx = x, ny = y + 1;
   if (!f[a[nx][ny]] && 0 <= ny && ny < cols && 0 <= nx && nx < rows) {
      f[a[nx][ny]] = true;
      srch(rows, cols, nx, ny, stp + 1);
      f[a[nx][ny]] = false;
   }

   nx = x, ny = y - 1;
   if (!f[a[nx][ny]] && 0 <= ny && ny < cols && 0 <= nx && nx < rows) {
      f[a[nx][ny]] = true;
      srch(rows, cols, nx, ny, stp + 1);
      f[a[nx][ny]] = false;
   }
}
void work1() {
   int n, k;
   scanf("%d%d", &n, &k);
   for (int i = 0; i < n; i++) scanf("%s", a[i]);
   srch(n, k, 0, 0, 1);
   printf("%d", ans);
}
int main() {
   work1();
   return 0;
}
