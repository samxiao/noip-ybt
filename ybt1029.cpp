#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

/*
1029：计算浮点数相除的余

【题目描述】
计算两个双精度浮点数a和b的相除的余数，a和b都是双精度浮点数。这里余数（r）的定义是：a=k×b+r，其中k是整数，0≤r<b。

【输入】
输入仅一行，包括两个双精度浮点数a和b。

【输出】
输出也仅一行，a÷b的余数。

【输入样例】
73.263 0.9973
【输出样例】
0.4601
*/
int main()
{
   double a, b, r;
   long long k;
   cin >> a >> b;
   k = a / b;
   r = a - b * k;
   //if (r < 0)   r -= b; // 200505 昨晚LYL一次就AC。 今天重新似乎OJ更新了数据，此行不用也可以通过了。题目注明了0<=r<b。
   cout << r;
   return 0;
}

