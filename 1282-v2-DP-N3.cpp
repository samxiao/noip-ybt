//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, ans = -999991234, f[CAP_N][CAP_N], dat[CAP_N][CAP_N];

int maxRowSum(int *a, int len) {
   int res = -0x7fffffff;
   int dp[len + 3] = {0};
   for (int i = 1; i <= len; i++) {
      dp[i] = dp[i - 1] < 0 ? a[i] : a[i] + dp[i - 1];
      if (res < dp[i]) res = dp[i];
   }
   //for (int i = 1; i <= len; i++) a[i] = dp[i];
   return res;
}

void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
         scanf("%d", &dat[i][j]);
      }
   }

   for (int i = 1; i <= n; i++) {
      memset(f, 0, sizeof(f));
      for (int j = i; j <= n; j++) {
         for (int k = 1; k <= n; k++) f[j][k] = f[j - 1][k] + dat[j][k];
         ans = max(ans, maxRowSum(f[j], n));
      }
   }

   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
