//SamXIAO
#include <bits/stdc++.h>
using namespace std;

bool judge(int x)
{
	int sum = 0;
	for(int i = 1; i < x; i++){
		if(0 == x % i) sum += i;
	}
	return sum == x;
}
void w1()
{
	int n ;
	scanf("%d", &n);
	for(int i = 2; i <= n; i++){
		if(judge(i)) printf("%d\n", i);
	}
}

int main()
{
	w1();
  return 0;
}
