#include <bits/stdc++.h>
using namespace std;

#define S64 signed long long
#define MOD_N 200907

int main() {
   S64 a1, a2, a3, k, t, ans;
   scanf("%lld", &t);
   while (t--) {
      scanf("%lld%lld%lld%lld", &a1, &a2, &a3, &k);
      if (a2 - a1 == a3 - a2) {
         ans = a1 + (k - 1) % MOD_N * (a2 - a1);
         printf("%lld\n", ans);
      } else {
         a3 = a2 / a1 % MOD_N;
         ans = a1;
         k--;
         while (k) {
            if (k & 1) ans = ans * a3 % MOD_N;
            a3 = a3 * a3 % MOD_N;
            k >>= 1;
         }
         printf("%lld\n", ans);
      }
   }
   return 0;
}
