#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT(x) (sizeof(x) / sizeof(x[0]))
char a[102][102];
bool fcols[100];
int num, ans, n, k;

void init() {
   memset(a, 0, sizeof(a));
   memset(fcols, 0, sizeof(fcols));
   num = 0;
   ans = 0;
}

void search(int rowNo) {
   if (num == k) {
      ans++;
      return;
   }
   if (rowNo > n) return;
   for (int i = 1; i <= n; i++) {
      if (!fcols[i] && '#' == a[rowNo][i]) {
         fcols[i] = true;
         num++;
         search(rowNo + 1); // current line selected
         num--;
         fcols[i] = false;
      }
   }
   search(rowNo + 1); // current line not selected
}

void work1() {
   while (true) {
      init();
      scanf("%d%d", &n, &k);
      if (-1 == n && -1 == k) break;
      for (int i = 0; i < n; i++) {
         scanf("%s", &a[i + 1][1]);
      }
      search(1);
      printf("%d\n", ans);
   }
}
int main() {
   work1();
   return 0;
}
