//SamXIAO
#include <bits/stdc++.h>
using namespace std;

double getni(int n)
{
	double ni = 1.0 / sqrt(3.0);
	for(int i = 1; i < n; i++){
		ni /= 3;
	}
	return ni / (2 * n - 1);
}

void w1()
{
	double ret = 0, ni ;
	int i = 1;
	while((ni = getni(i)) > 1e-6){
		ret += (i % 2) ? ni : -ni;
		i++;
	}
	printf("%.10lf", 6.0*ret);
}

int main()
{
	w1();
  return 0;
}
