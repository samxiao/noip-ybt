#include <bits/stdc++.h>
using namespace std;

#define S64 signed long long

int main() {
   S64 n, m, k, x, base = 10, ans = 1;
   scanf("%lld%lld%lld%lld", &n, &m, &k, &x);
   while (k) {
      if (k & 1) ans = ans * base % n;
      base = base * base % n;
      k >>= 1;
   }
   printf("%lld", (m * ans + x) % n);
   return 0;
}
