#include <iostream>
#include <bits/stdc++.h>

using namespace std;

#define szARR(x) sizeof(x) / sizeof(x[0])
#define DBG_PRINTF

char op[1000];
int num[1000], topOp, topNum;

struct op_ele_t
{
	bool isOp;
	union dat_t {
		int x;
		char op;
	} dat;
};

struct op_quene_t
{
	vector<op_ele_t> q;
	void pushNum(int x)
	{
		op_ele_t e;
		e.dat.x = x;
		e.isOp = false;
		q.push_back(e);
	}

	bool pushOp(char x)
	{
		op_ele_t e;
		e.dat.op = x;
		e.isOp = true;
		q.push_back(e);
	}
};

int GetOpPriInStack(char c)
{
	// '(' 栈外优先级最高，栈内优先级最低。 ‘)'不可能出现在栈内
	const char *priset[] =  {"(", "+-", "*xX/%", "^", ")"};
	int i;
	for (i = 0; i < szARR(priset); i++)
	{
		for (int j = 0; j < strlen(priset[i]); j++)
			if (c == priset[i][j]) return i;
	}

	return 0;
}

int GetOpPriOutStack(char c)
{
	// '(' 栈外优先级最高，栈内优先级最低。‘)'不可能出现在栈内
	const char *priset[] = {")", "+-", "*xX/%", "^", "("};
	int i;
	for (i = 0; i < szARR(priset); i++)
	{
		for (int j = 0; j < strlen(priset[i]); j++)
			if (c == priset[i][j]) return i;
	}

	return 0;
}

//  栈外运算符和栈内运算符优先级比较
int CmpOp(char cOut, char cIn)
{
	int n1, n2;
	n1 = GetOpPriOutStack(cOut);
	n2 = GetOpPriInStack(cIn);
	if      (n1 >  n2) return  1;
	else if (n1 == n2) return  0;
	else    		       return -1;
}

int calculate(char c, int x1, int x2)
{
	int r = 0;
	if('+' == c) return x1 + x2;
	if('-' == c) return x1 - x2;
	if('*' == c) return x1 * x2;
	if('/' == c) return x1 / x2;
	if('%' == c) return x1 % x2;
	if('^' == c) return pow(x1, x2);

	return r;
}

void ToSuffix(string sIn, op_quene_t *suffExpr)
{
	bool hasNum = false;
	int x = 0;
	op_ele_t ele;
	for (int i = 0; i < sIn.length(); i++)
	{
		char theChar = sIn[i];
		if (isdigit(theChar)) x = x * 10 + theChar - '0', hasNum = true;
		else
		{
			if (hasNum)	suffExpr->pushNum(x);
 			x = 0, hasNum = false;
			if (topOp)
			{
				if (theChar == ')')
				{
					char op2;
					while ((op2 = op[--topOp]) != '(') suffExpr->pushOp(op2);
				}
				else if (CmpOp(theChar, op[topOp - 1]) <= 0)
				{
					while (topOp){
						if(CmpOp(theChar, op[topOp - 1]) > 0) break;
						suffExpr->pushOp(op[--topOp]);
					}
				}
			}
			if (theChar != ')') op[topOp++] = theChar;
		}
	}
	if (hasNum)	suffExpr->pushNum(x);
	while (topOp) suffExpr->pushOp(op[--topOp]);
	//suffExpr->print();
}

int CalcSuffix(op_quene_t *suffExpr)
{
	int r = 0;
	for (int i = 0; i < suffExpr->q.size(); i++)
	{
		op_ele_t ele = suffExpr->q[i];
		if (ele.isOp)
		{
			int x1, x2;
			x2 = num[--topNum];
			x1 = num[--topNum];
			r = calculate(ele.dat.op, x1, x2);
			num[topNum++] = r;
		}
		else
		{
			num[topNum++] = ele.dat.x;
		}
	}
	return r;
}

char sIn[10000];
op_quene_t qnSuff;

int main()
{
	int a;
	cin >> sIn;
	ToSuffix(sIn, &qnSuff);
	int r = CalcSuffix(&qnSuff);
	printf("%d", r);
	return 0;
}

void tc1()
{
	/*
	23-3+3*2*4+100 = 144.    >>> 23 3 - 3 2 * 4 * + 100 +
	23-8*2+6*3+3*10 = 55
	23+3-8*2+7^2+5 = 64
	(100+2)*401+301=41,203                >>> 100 2 + 401 * 301 + 4
	(100+2)*401+5/(6+100*401)+301 = 41203 >>> 100 2 + 401 * 5 6 100 401 * + / + 301 +
	(A+B)*D+E/(F+A*D)+3  >>> AB+D*EFAD*+/+C+
	1+(3+2)*(7^2+6*9)/(2)  = 258
	*/
	const string s1 = "(100+2)*401+5/(6+100*401)+301"; // = 41203
	const string s2 = "1+2*(3-4)/(5-6)";					// =3

	cout << s1 << endl;
	ToSuffix(s1, &qnSuff);
	int r = CalcSuffix(&qnSuff);
	printf(" ans=%d", r);
}
