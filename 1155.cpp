//SamXIAO
#include <bits/stdc++.h>
using namespace std;

bool isprime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}

void w1()
{
	for(int i = 1; i <= 9 ; i++){
		for(int j = 0; j <= 9; j++){
			int b = i * 100 + j * 10 + i;
			if(isprime(b))
				printf("%d\n", b);
		}
	}
}

int main()
{
	w1();
  return 0;
}
