//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

bool dg(int k, int x) {
   if (k == x) return true;
   if (x < k) return false;
   return dg(k * 2 + 1, x) || dg(k * 3 + 1, x);
}

void work1() {
   long long k, x;
   scanf("%lld,%lld", &k, &x);
   printf("%s", dg(k, x) ? "YES" : "NO");
}

int main() {
   work1();
   return 0;
}
