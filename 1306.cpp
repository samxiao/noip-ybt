//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 1086
#define LIMIT_MAX 0x1ffffff
int n, ans, f[CAP_N][CAP_N], cnt;
int a[CAP_N], b[CAP_N];
int d[CAP_N];
int la, lb;

int LCS(int *a, int *b) {
   memset(f, 0, sizeof(f));
   memset(d, 0, sizeof(d));
   cnt = 0;
   int res = 0;
   for (int i = 1; i <= la; i++) {
      for (int j = 1; j <= lb; j++) {
         f[i][j] = max(f[i - 1][j], f[i][j - 1]);
         if (a[i - 1] == b[j - 1]) {
            if (f[i - 1][j - 1] + 1 > f[i][j]) {
               f[i][j] = f[i - 1][j - 1] + 1;
            }
            if (res < f[i][j]) {
               res = f[i][j];
               d[++cnt] = a[i - 1];
            }
         }
      }
   }
   return res;
}

int e[CAP_N], ecnt;

int LIS(int *dat, int n) {
   int res = 0, f[CAP_N] = {0};
   for (int i = 1; i <= n; i++) {
      f[i] = 1;
      for (int j = 1; j < i; j++) {
         if (dat[i] > dat[j] && f[j] + 1 > f[i]) {
            f[i] = f[j] + 1;
            if (res < f[i]) {
               res = f[i];
               e[i] = j;
               ecnt = i;
               DBG_PRINT(" ..j=%d(%d)<i=%d(%d) ", j, d[e[i]], i, d[ecnt]);
            }
         }
      }
   }

   printf("%d\n", res);
   return res;
}

void work1() {
   scanf("%d", &la);
   for (int i = 0; i < la; i++) scanf("%d", &a[i]);
   scanf("%d", &lb);
   for (int i = 0; i < lb; i++) scanf("%d", &b[i]);

   LCS(a, b);
   LIS(d, cnt);
   for (int i = 1; i <= cnt; i++) DBG_PRINT("//%d%s", d[i], i == cnt ? "\n" : " ");
   DBG_PRINT("--%d:%d", ecnt, d[ecnt]);
   stack<int> sp;
   sp.push(d[ecnt]);
   for (int i = ecnt; e[i]; i = e[i]) DBG_PRINT("i=%d,%d:%d ", i, e[i], d[e[i]]);
   for (int i = ecnt; e[i]; i = e[i]) sp.push(d[e[i]]);
   while (!sp.empty()) printf("%d ", sp.top()), sp.pop();
}

int main() {
   work1();
   return 0;
}

/*
5
1 4 2 5 -12
5
-12 1 2 4 5

3
1 2 5 (1 4 5)



7
8 9 1 4 2 5 -12
7
7 8 -12 1 2 4 5


*/