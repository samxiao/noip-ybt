//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define MAXNN 10086

int cnt;
char s1[202];

void work1()
{
	string x;
	char buf[220]={0};
	getline(cin, x);
	int pos = 0;
	string so, sn;
	cin >> so >> sn;
	while(sscanf(x.c_str() + pos, "%s", buf) != EOF){
		pos += strlen(buf)+1; // 只能处理单词间有一个空格
		if(!strcmp(buf, so.c_str())) printf("%s ", sn.c_str());
		else printf("%s ", buf);
		memset(buf, 0, sizeof(buf));
	}
}

int main()
{
	work1();
  return 0;
}
