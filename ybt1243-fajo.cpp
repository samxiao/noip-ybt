#include <bits/stdc++.h>
using namespace std;

int a[100086];
int n, m, ans = 0x7fff0000;

bool TooSmall(int exp)
{
   int sum = 0, cnt = 0;
   for (int i = 0; i < n; i++)
   {
      if(sum + a[i] > exp){
         sum = a[i];
         if (++cnt >= m)  return true;
      }
      else sum += a[i];
   }
   return false;
}

void work()
{
   int left = 0, right = 0, mid;
   for (int i = 0; i < n; i++)
   {
      left = max(a[i], left);
      right += a[i];
   }
   while (left <= right)
   {
      mid = (left + right) / 2;
      if (TooSmall(mid)) left  = mid + 1; // 这个MID一定不是答案
      else               right = mid - 1, ans = mid; // 这个MID可能是答案,但最后一个MID值一定是答案.
   }
   printf("%d", ans);
}

int main()
{
   scanf("%d%d", &n, &m);
   for (int i = 0; i < n; i++) scanf("%d", &a[i]);
   work();
   return 0;
}
