//SamXIAO
#include <bits/stdc++.h>
using namespace std;

void w1()
{
	int n, ans=0;
	scanf("%d", &n);
	while(n--){
		int x;
		scanf("%d", &x);
		ans += x / 70;
		if(x % 70) ans++;
	}
	printf("%.1f", ans*0.1);
}

int main()
{
	w1();
  return 0;
}
