//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

long long dg(int n, int k) {
   if (0 == k)
      return n;
   dg(k, n % k);
}

void work1() {
   long long n, k;
   scanf("%lld%lld", &n, &k);
   printf("%lld\n", dg(max(n, k), min(n, k)));
}

int main() {
   work1();
   return 0;
}
