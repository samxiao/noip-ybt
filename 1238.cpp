//Sam.XIAO
#include <bits/stdc++.h>
using namespace std;

inline double check(double a, double b, double c, double d, double x) {
   double res = 0;
   res += a * x * x * x;
   res += b * x * x;
   res += c * x;
   res += d;

   return res;
}

void work_V1() {
   double a, b, c, d;
   scanf("%lf%lf%lf%lf", &a, &b, &c, &d);
   long long tot = 0;

   int l = -100 * 1000, r = -l, mid;
   int cnt = 0;

   double x = -100.0;
   while (x <= 100) {
      double y1, y2, y3;
      y1 = check(a, b, c, d, x - 0.005);
      y2 = check(a, b, c, d, x + 0.005);
      if (0 == y1 || y1 * y2 < 0)
         printf("%.2lf ", x), x += 1;
      else
         x += 0.005;
   }
}

// v2
void work1() {
   double a, b, c, d;
   scanf("%lf%lf%lf%lf", &a, &b, &c, &d);
   double x = -100.0;

   while (x <= 100) {
      double l = x, r = x + 1, mid;
      bool isFnd = false;
      while (l <= r) {
         mid = (l + r) / 2;
         double y1, y2, xx = mid;
         y1 = check(a, b, c, d, xx - 0.005);
         y2 = check(a, b, c, d, xx + 0.005);
         if (0 == y1 || 0 == y2 || y1 * y2 < 0) {
            printf("%.2lf ", xx);
            x = mid + 1;
            isFnd = true;
            break;
         } else {
            if (0 < y1) {
               if (y1 < y2)
                  r = mid - 0.001;
               else
                  l = mid + 0.001;
            } else {
               if (y1 < y2)
                  l = mid + 0.001;
               else
                  r = mid - 0.001;
            }
         }
      }
      if (!isFnd) x += 1;
   }
}

int main() {
   work1();
   return 0;
}
