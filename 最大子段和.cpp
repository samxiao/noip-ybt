//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 1
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, ans = -999991234, f[CAP_N], dat[CAP_N];

void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) scanf("%d", &dat[i]);
   for (int i = 1, tmp = 0; i <= n; i++) {
#if 0
      if (tmp < 0) tmp = 0;
      tmp += dat[i];
      if (ans < tmp) ans = tmp;
#else
      f[i] = f[i - 1] < 0 ? dat[i] : f[i - 1] + dat[i];
      if (ans < f[i]) ans = f[i];
#endif
   }
   for (int i = 1; i <= n; i++) DBG_PRINT("%4d%s", dat[i], i == n ? "\n" : " ");
   for (int i = 1; i <= n; i++) DBG_PRINT("%4d%s", f[i], i == n ? "\n" : " ");
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}

/*
8
4  -3  5  -2  -1  2  6  -2

11
==============================================================
6 
-2 11 -4 13 -5 -2

20
==============================================================
12
  -2   11   -4   13   -5   -2   -3    4  -99    1   -5    9

  -2   11    7   20   15   13   10   14  -85    1   -4    9
20
==============================================================

*/