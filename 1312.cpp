//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

long long f[MAX_LEN + 1];
long long l[MAX_LEN + 1];
int z, x, y;
long long step() {
   f[1] = 1;
   //for (int i = 0; i < szCNT_OF(f); i++) f[i] = 1;
   for (int i = 2; i <= z + 1; i++) {
      if (i - x >= 1) l[i] = f[i - x] * y;
      f[i] = f[i - 1] + l[i - 2];
   }
   return f[z + 1];
}

void work1() {
   scanf("%d%d%d", &x, &y, &z);

   printf("%lld", step());
}

int main() {
   work1();
   return 0;
}
