//Sam.XIAO
#include <bits/stdc++.h>
using namespace std;

void work1() {
   long long b, p, k;
   scanf("%lld%lld%lld", &b, &p, &k);

   long long ans = 1, b2 = b % k, p2 = p;

   while (p2) {
      if (p2 % 2) {
         ans *= b2;
         ans %= k;
      }
      b2 *= b2;
      b2 %= k;
      p2 /= 2;
   }
   printf("%lld^%lld mod %lld=%lld", b, p, k, ans);
}

int main() {
   work1();
   return 0;
}
