//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAXLEN 5000

long long baseN;

class HPNum {
  private:
   struct num_t {
      long long x[MAXLEN];
      int len;
   };
   num_t nums;

   static long long getBaseN(int len) {
      long long x = 1;
      while (len--) x *= 10;

      return x;
   }

   static inline int comb(long long *num, int len) {
      long long res = 0;
      while (len) {
         res *= 10;
         res += num[--len];
      }
      return res;
   }

   static void zip(num_t *num) {
      baseN = getBaseN(BASE_N_LEN);
      int l = num->len / BASE_N_LEN, lhead = BASE_N_LEN;
      if (num->len % BASE_N_LEN) l++;
      int pos = 0;
      for (int i = 0; i < l; i++) {
         if (i == l - 1 && num->len % BASE_N_LEN) lhead = num->len % BASE_N_LEN;
         num->x[i] = comb(&num->x[pos], lhead);
         pos += lhead;
      }
      //for (int i = l; i < MAXLEN; i++) num->x[i] = 0;
      num->len = l;
   }

   static void str2num(char *s, num_t *num) {
      int pos = 0;
      while (s[pos] == '0' && pos < strlen(s)) pos++;
      num->len = strlen(s) - pos;
      for (int i = strlen(s) - 1, j = 0; i >= pos; i--, j++)
         num->x[j] = s[i] - '0';

      zip(num);
   }
   void clear() {
      memset(nums.x, 0, sizeof(nums.x));
      getBaseN(BASE_N_LEN);
   }

  public:
   HPNum(char *a1) {
      clear();
      str2num(a1, &nums);
   }

   HPNum(num_t b) {
      clear();
      nums.len = b.len;
      for (int i = 0; i < b.len; i++)
         nums.x[i] = b.x[i];
   }

   HPNum() {
      clear();
   }

   void operator=(const HPNum b) {
      clear();
      nums.len = b.nums.len;
      for (int i = 0; i < b.nums.len; i++)
         nums.x[i] = b.nums.x[i];
   }

   HPNum operator+(const HPNum b) {
      HPNum const x1(nums);
      HPNum x3;

      int lc = max(b.nums.len, x1.nums.len);
      for (int i = 0; i < lc; i++) {
         x3.nums.x[i] += b.nums.x[i] + x1.nums.x[i];
      }
      int pos = 0;
      while (pos < x1.nums.len + 6) {
         x3.nums.x[pos + 1] += x3.nums.x[pos] / baseN;
         x3.nums.x[pos] %= baseN;
         pos++;
      }

      int cnt = x1.nums.len + 6;
      while (!x3.nums.x[cnt]) {
         cnt--;
      }
      x3.nums.len = cnt + 1;
      return x3;
   }

   HPNum operator*(HPNum b) {
      HPNum const x1(nums);
      HPNum x3;

      for (int i = 0; i < x1.nums.len; i++) {
         if (0 == x1.nums.x[i]) continue;
         for (int j = 0; j < b.nums.len; j++) {
            x3.nums.x[i + j] += x1.nums.x[i] * b.nums.x[j];
         }
      }
      int pos = 0;
      while (pos < x1.nums.len + b.nums.len + 6) {
         x3.nums.x[pos + 1] += x3.nums.x[pos] / baseN;
         x3.nums.x[pos] %= baseN;
         pos++;
      }

      int cnt = x1.nums.len + b.nums.len + 6;
      while (!x3.nums.x[cnt]) {
         cnt--;
      }
      x3.nums.len = cnt + 1;
      return x3;
   }

   HPNum operator*(const int b) {
      HPNum const x1(nums);
      HPNum x3;

      for (int i = 0; i < x1.nums.len; i++) {
         if (0 == x1.nums.x[i]) continue;
         x3.nums.x[i] += x1.nums.x[i] * b;
      }
      int pos = 0;
      while (pos < x1.nums.len + 6) {
         x3.nums.x[pos + 1] += x3.nums.x[pos] / baseN;
         x3.nums.x[pos] %= baseN;
         pos++;
      }

      int cnt = x1.nums.len + 6;
      while (!x3.nums.x[cnt]) {
         cnt--;
      }
      x3.nums.len = cnt + 1;
      return x3;
   }

#define _TO_STRING(x) #x
#define FMT00__ "lld"
#define FMT0 "%" FMT00__
#define FMT1(x) "%0"_TO_STRING(x)FMT00__
   void print() {
      int pos = nums.len - 1;
      while (pos >= 0 && nums.x[pos] == 0) pos--;
      // char fmt[30] = {0};
      // sprintf(fmt, "%%0%dd", BASE_N_LEN);
      for (int i = pos; i >= 0; i--)
         printf(pos == i ? FMT0 : FMT1(BASE_N_LEN), nums.x[i]);
   }

   ~HPNum() {}
};

void work1() {
   char s1[MAXLEN] = {0}, s2[MAXLEN] = {0};
   int n;
   memset(s1, 0, sizeof(s1));
   memset(s2, 0, sizeof(s2));
   scanf("%s%s", s1, s2);

   HPNum a(s1), b(s2);

   // a.print(), printf("..\n");
   // b.print(), printf("..\n");
   (a * b).print();
}

int main() {
   work1();
   return 0;
}
