#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

double f[100];
int main()
{
	int n;
	scanf("%d", &n);
	int x, y;
	for(int i=0; i<n; i++){
		scanf("%d%d", &x, &y);
		f[i] = y / (double)x;
		if(i>0){
			printf("%s\n", f[i] - f[0] > 0.05 ? "better" :
					(f[0] - f[i] > 0.05 ? "worse" : "same"));
		}
	}
  return 0;
}
