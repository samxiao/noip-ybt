//SamXIAO
#include <bits/stdc++.h>
using namespace std;


bool isprime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}
int f[10000], cnt;
void gen()
{
	for(int i = 3; i < 10002; i += 2)
		if(isprime(i)) f[cnt++] = i;
}

void work1()
{
	int m, n, pos=1, ans = 0;
	scanf("%d", &n);
	gen();
	for(int i = 1; i < cnt; i++){
		if(f[i] <= n && f[i] - f[i-1] == 2) printf("%d %d\n", f[i-1], f[i]);
	}
}

int main()
{
	work1();
  return 0;
}
