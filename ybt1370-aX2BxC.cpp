#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long

#define MAXN 1000
#define MAXW 1000
#define uint64_t unsigned long long


struct abc_t_v1
{
   int a, b, c, no;
   uint64_t CalcY(int x)
   {
      return a * x * x + b * x + c;
   }
};


//v2 demo for priority_queue customer struct.
struct abc_t_v2
{
   int a, b, c, no;
   uint64_t x, y;
   friend bool operator<(abc_t_v2 d1, abc_t_v2 d2)
   {
      return d1.y > d2.y;
   }

   friend bool operator<=(abc_t_v2 d1, abc_t_v2 d2)
   {
      return d1.y <= d2.y;
   }
   abc_t_v2(){};
   abc_t_v2(int a, int b, int c, int x) : a(a), b(b), c(c), x(x)
   {
      CalcY();
   };
   void CalcY()
   {
      y = a * x * x + b * x + c;
   }
};


void work_v2( )
{
   int m, n;
   scanf("%d%d", &n, &m);
   abc_t_v2 parm[10086];
   priority_queue<abc_t_v2> h1;
   priority_queue<abc_t_v2> h2;
   int cnt = 0, xCur = 1;
   for (int i = 0; i < n; ++i)
      scanf("%d%d%d", &parm[i].a, &parm[i].b, &parm[i].c), parm[i].no = i;

   while (cnt < m)
   {
      for (int i = 0; i < n; ++i)
      {
         parm[i].x = xCur;
         parm[i].CalcY();
         h1.push(parm[i]);
      }

      xCur++;
      abc_t_v2 x1 = h1.top();
      while (h2.size())
      {
         abc_t_v2 x2 = h2.top();
         if (x2 <= x1)
         {
            printf("%lld ", x2.y);
            h2.pop();
            cnt++;
            if (cnt >= m)
               break;
         }
         else
            break;
      }
      while (h1.size())
      {
         h2.push(h1.top());
         h1.pop();
      }
   }
}

// RONGYANG WJR: 使用大根堆，堆的尺寸开销最大为M, 小根堆可能是M的几十倍，时间也可能10倍以上。
void work_v1( )
{
   //priority_queue<int, vector<int>, less<int> > h;  // big root heap , default.
   //priority_queue<uint64_t, vector<uint64_t>, greater<uint64_t> > h1; // little root heap
   //priority_queue<uint64_t, vector<uint64_t>, greater<uint64_t> > h2; // little root heap

   priority_queue<uint64_t, vector<uint64_t>, greater<uint64_t> > h1;
   priority_queue<uint64_t, vector<uint64_t>, greater<uint64_t> > h2;
   abc_t_v1 parm[10086];
   int m, n;
   scanf("%d%d", &n, &m);   
   for(int i=0; i<n; i++) scanf("%d%d%d", &parm[i].a, &parm[i].b, &parm[i].c);

   int cnt=0, xCur=1;
   while(cnt < m){
      for(int i=0; i<n; ++i) h1.push(parm[i].CalcY(xCur));
      uint64_t x1 = h1.top();
      xCur++;
      while(h2.size()){
         uint64_t x2 = h2.top();
         if(x2 <= x1){
            printf("%d ", x2);
            h2.pop();
            if(++cnt >=m) break;
         }
         else break;
      }
      while(h1.size()) h2.push(h1.top()), h1.pop();
   }
}

int main()
{
   work_v1();
   //work_v2();

   return 0;
}
