// AC@LG;  AC@YBT
#include <bits/stdc++.h>
using namespace std;

#define NBDAT 100086
int dat[NBDAT], n, m;
long long sum[NBDAT * 4];

#define GET_LEFT_NODE(x)  (2 * (x) + 1)
#define GET_RIGHT_NODE(x) (2 * (x) + 2)

void update(int node, int l, int r, int idx, int delta) {
   if (idx < l || r < idx) return;
   if (l == r && l == idx) {
      dat[idx] += delta;
      sum[node] += delta;
      return;
   }
   int mid = l + (r - l) / 2;
   if (l <= idx && idx <= mid)
      update(GET_LEFT_NODE(node), l, mid, idx, delta);
   else
      update(GET_RIGHT_NODE(node), mid + 1, r, idx, delta);
   sum[node] = sum[GET_LEFT_NODE(node)] + sum[GET_RIGHT_NODE(node)];
}

void update(int idx, int delta) { update(0, 0, n, idx, delta); }

long long getsum(int node, int l, int r, int qL, int qR) {
   if (qR < l || r < qL) return 0;
   if (qL <= l && r <= qR) return sum[node];
   int mid = l + (r - l) / 2;
   long long ans = getsum(GET_LEFT_NODE(node), l, mid, qL, qR);
   ans += getsum(GET_RIGHT_NODE(node), mid + 1, r, qL, qR);
   return ans;
}

long long getsum(int l, int r) { return getsum(0, 0, n, l, r); }

int read() {
   int ret = 0;
   char ch = getchar();
   int k = 1;
   while (!('0' <= ch && ch <= '9')) {
      if (ch = '-') k = -1;
      ch = getchar();
   }

   while ('0' <= ch && ch <= '9') {
      ret = ret * 10 + ch - '0';
      ch = getchar();
   }
   return k * ret;
}

int main() {
   n = read();
   m = read();
   int k, a, b;
   for (int i = 0; i < m; i++) {
      k = read();
      a = read();
      b = read();
      if (0 == k)
         update(a, b);
      else
         printf("%lld\n", getsum(a, b));
   }

   return 0;
}
