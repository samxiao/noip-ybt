
g++ -S %1.cpp
g++ -g -c %1.cpp
objdump -S %1.o > %1_objdump.s

g++ -c -g -Wa,-adlhn %1.cpp > %1-2.s
goto L_end

================以下所有为说明======================
第一种方式：
1、
g++-S hello.cpp
g++ -g -c hello.cpp
生成.s汇编文件和.o文件
   
 

2、第一步中必须要使用第二条命令，第一条命令只是为了显示.s汇编文件
objdump -S hello.o > hello_objdump.s
生成含有调试信息、CPP源代码的汇编代码

 
https://www.cnblogs.com/fengkang1008/p/4652193.html

 
GCC输出带C源代码的汇编文件

第二种方式：

使用GNU C Assembler的列表功能来完成，例如：

g++ -c -g -Wa,-adlhn hello.cpp > gnu.s
 
g++ -c -g -Wa,-adlhn asm-test.cpp > asm-test.s

这个命令的说明如下： 
-Wa,option ：把选项option传递给汇编器.如果option含有逗号,就在逗号处分割成多个选项.也就是Gas，至于Gas的命令参数，可以查看相应的文档，其中-a[cdghlns]参数的作用是打开列表功能。

这种方式可以显示足够的信息，但是命令稍微复杂，参数比较多，不太容易选择。


L_end: