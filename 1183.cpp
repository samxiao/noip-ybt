//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 5500  // 10 * 1000 * 1000 + 86

struct h_t {
   char id[15];
   int yo, no;
   h_t() { memset(id, 0, sizeof(id)); }
};

h_t old[MAX_LEN], dat[MAX_LEN];
bool cmpold(h_t x1, h_t x2) {
   if (x1.yo == x2.yo) return x1.no < x2.no;
   return x1.yo > x2.yo;
}

bool cmp(h_t x1, h_t x2) { return x1.no < x2.no; }
void work1() {
   int n, cntold = 0, cnt = 0;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      int x;
      char fm[10];
      scanf("%s%d", fm, &x);
      if (x >= 60) {
         old[cntold].yo = x;
         memcpy(old[cntold].id, fm, strlen(fm));
         old[cntold].no=cntold++;
      } else {
         dat[cnt].yo = x;
         memcpy(dat[cnt].id, fm, strlen(fm));
         cnt++;
      }
   }

   //sort(dat, dat + cnt, cmp);
   sort(old, old + cntold, cmpold);

   for (int i = 0; i < cntold; i++) {
      printf("%s\n", old[i].id);
   }

   for (int i = 0; i < cnt; i++) {
      printf("%s\n", dat[i].id);
   }
}

int main() {
   work1();
   return 0;
}
