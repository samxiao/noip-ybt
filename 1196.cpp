//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

long long f[MAX_LEN + 1];

long long step(int n) {
   f[0] = 1;
   f[1] = 3;
   for (int i = 2; i <= n; i++) f[i] = 2 * f[i - 1] + f[i - 2];
   return f[n];
}

void work1() {
   int n;
   scanf("%d", &n);

   printf("%lld", step(n));
}

int main() {
   work1();
   return 0;
}
