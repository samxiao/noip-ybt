#include <bits/stdc++.h>
//BIT: Binary index Tree. fenwick tree
#define LOWBIT(x) x &(-x)

struct fenwick_t
{
   int n;
   int *c;
   int *d;

   fenwick_t(int N, int *ptrData, int *ptrC) { n = N, d = ptrData, c = ptrC; }
   void update(int x, int delta)
   {
      while (x <= n)
         c[x] += delta, x += LOWBIT(x);
   }

   int sum(int x)
   {
      int ans = 0;
      while (x)
         ans += c[x], x -= LOWBIT(x);
      return ans;
   }

   int getsec(int l, int r)
   {
      return sum(r) - sum(l - 1);
   }
};
 

main()
{
 
   return 0;
}