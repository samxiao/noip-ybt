#include <bits/stdc++.h>
using namespace std;

struct ele_t {
   int price, value, qty;
};
ele_t dat[503];
int f[6003];

int main() {
   int n, m;
   scanf("%d%d", &n, &m);
   for (int i = 1; i <= n; i++)
      scanf("%d%d%d", &dat[i].price, &dat[i].value, &dat[i].qty);
   for (int i = 1; i <= n; i++)
      for (int j = m; j >= 0; j--) {
         for (int k = 0; k <= dat[i].qty; k++) {
            if (j - dat[i].price * k < 0) break;
            f[j] = max(f[j], f[j - dat[i].price * k] + dat[i].value * k);
         }
      }

   printf("%d", f[m]);
   return 0;
}