#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int n, x, ans=-999;
	scanf("%d",  &n);
	for(int i=0; i<n; i++){
		scanf("%d", &x);
		if(ans < x) ans = x;
	}
	printf("%d", ans);
  return 0;
}
