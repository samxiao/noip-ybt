//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

char ch[1000];

int gcd(int m, int n) {
   if (0 == m % n) return n;
   return gcd(n, m % n);
}

int lcm(int m, int n) { return m * n / gcd(m, n); }

void work1() {
   int n;
   scanf("%d", &n);
   int z[100], m[100];
   for (int i = 0; i < n; i++) {
      scanf("%d/%d", &z[i], &m[i]);
   }
   for (int i = 1; i < n; i++) {
      int q = lcm(m[i], m[i - 1]);
      z[i] = z[i] * q / m[i] + z[i - 1] * q / m[i - 1];
      m[i] = q;
   }
   int q = gcd(z[n - 1], m[n - 1]);
   z[n - 1] /= q;
   m[n - 1] /= q;
   if (1 == m[n - 1])
      printf("%d", z[n - 1]);
   else
      printf("%d/%d", z[n - 1], m[n - 1]);
}

int main() {
   work1();
   return 0;
}
