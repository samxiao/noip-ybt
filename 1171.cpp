//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N 10000

char theN[100];
bool div9(char *theN, int x) {
   int lc = strlen(theN);
   int res = 0;
   for (int i = 0; i < lc; i++) {
      if (0 == i)
         res = (theN[i] - '0') % x;
      else
         res = (res * 10 + theN[i] - '0') % x;
   }

   return 0 == res;
}
void work1() {
   scanf("%s", theN);
   int cnt = 0;

   for (int i = 2; i <= 9; i++) {
      if (div9(theN, i)) {
         cnt++;
         printf("%d ", i);
      }
   }

   if (0 == cnt) printf("none");
}

int main() {
   work1();
   return 0;
}
