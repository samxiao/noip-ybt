//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

long long f[MAX_LEN + 1][MAX_LEN + 1];

long long part(int m, int k) {
   if (1 == k || 0 == m) return 1;
   if (m < k)
      return part(m, m);
   else
      return part(m - k, k) + part(m, k - 1);
}

void work1() {
   int n, k, t;
   scanf("%d", &t);

   for (int i = 0; i < t; i++) {
      scanf("%d%d", &n, &k);

      printf("%lld\n", part(n, k));
   }
}

int main() {
   work1();
   return 0;
}
