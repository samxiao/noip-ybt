//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 209
#define LIMIT_MAX 0x1ffffff
int n, ans, di[CAP_N][CAP_N];
int f[CAP_N], w[CAP_N], pre[CAP_N];
int sp[CAP_N * CAP_N], top = 0;

void work1() {
   //freopen("1262-mine8.in", "r", stdin);
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) scanf("%d", &w[i]);
   int x, y;
   while (scanf("%d%d", &x, &y) != EOF) {
      if (0 == x && 0 == y) break;
      di[x][y] = 1;
   }

   int lastPoint = 0, root = 1;
   for (int i = 1; i <= n; i++) {
      lastPoint = 0;
      for (int j = 1, maxw = 0; j < i; j++) {
         if (di[j][i] && maxw <= f[j]) lastPoint = j, maxw = f[j];
      }
      f[i] = f[lastPoint] + w[i];
      pre[i] = lastPoint;
      if (ans < f[i]) {
         ans = f[i];
         root = i;
         DBG_PRINT("%d. ", lastPoint);
      }
   }

   sp[top++] = root;
   for (int i = root; pre[i]; i = pre[i]) sp[top++] = pre[i];
   for (int cnt = 0; top;) printf("%s%d", cnt++ ? "-" : "", sp[--top]);
   printf("\n%d", ans);
}

int main() {
   work1();
   return 0;
}
