#include <bits/stdc++.h>
using namespace std;

#define MAXX

bool cmp(string x1, string x2)
{
   return x1 < x2;
}

string sDat[100 * 1000];
int main()
{
   int n = 0, m, p, c;
   while (cin >> sDat[n++])
      ;
   n--;
   sort(sDat, sDat + n, cmp);
   int ans = sDat[0].length() + 1; // +root
   for (int i = 1; i < n; i++)
   {
      int j = 0;
      while (sDat[i - 1][j] == sDat[i][j] && j < sDat[i - 1].length())
         j++;
      ans += sDat[i].length() - j;
   }
   printf("%d", ans);
   return 0;
}
