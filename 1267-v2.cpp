//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int w[300], c[300], f[300];

int main() {
   int n, m;
   scanf("%d%d", &m, &n);
   for (int i = 1; i <= n; i++) scanf("%d%d", &w[i], &c[i]);
   for (int i = 1; i <= n; i++) {
      for (int j = m; j >= w[i]; j--) {
         f[j] = max(f[j], f[j - w[i]] + c[i]);
      }
   }

   printf("%d", f[m]);
   return 0;
}