#include <bits/stdc++.h>
using namespace std;

struct t1_t{
	int a;
	int *ptr;

};


int main()
{
	t1_t t, *ptr;
	t.a = 123;
	int x = 12345;
	ptr = &t;
	ptr->ptr=&x;
	printf("1: %d %d\n", x, *ptr->ptr);
	*ptr->ptr=5678;
	ptr->*ptr=5678;
	printf("2: %d %d \n",  x, *ptr->ptr);
	return 0;
}
