//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

char ch[1000];
struct stk_t {
   int pos;
   char ch;
};
stk_t stk[1000];
int rk[1000], cntR;
int top;
void dg(int n, int l) {
   if (l == n) return;
   if ('(' == ch[n]) {
      stk[top].ch = '(';
      stk[top].pos = n;
      top++;
   } else if (')' == ch[n]) {
      if (top <= 0)
         rk[cntR++] = n;
      else if (top && stk[--top].ch != '(')
         rk[cntR++] = n;

      if (top >= 0) stk[top].ch = 0;
   }
   dg(n + 1, l);
}

void work1() {
   while (scanf("%s", ch) != EOF) {
      printf("%s\n", ch);
      dg(0, strlen(ch));
      for (int i = 0; i < top; i++) ch[stk[i].pos] = '$';
      for (int i = 0; i < cntR; i++) ch[rk[i]] = '?';
      for (int i = 0; i < strlen(ch); i++)
         printf("%c", '?' == ch[i] || '$' == ch[i] ? ch[i] : ' ');
      printf("\n");
      memset(ch, 0, sizeof(ch));
      top = 0;
      cntR = 0;
   }
}

int main() {
   work1();
   return 0;
}
