#include <bits/stdc++.h>
using namespace std;

#define MAXX
int dat[20000];
int FindRoot(int n){
   for(int i=1; i<=n; i++) 
      if(0 == dat[i]) return i;
}


int cnt[2000];
int FindNodOfMaxChildren(int n){
   for(int i=1; i<=n; i++) cnt[dat[i]]++; 
   int nodeCnt = 0, r = 0;
   for(int i=1; i<=n; i++) 
      if(nodeCnt < cnt[i] ){
         nodeCnt = cnt[i];
         r = i;
      }
   return r;
}

void PrintChildren(int node, int n){
   for(int i=1; i<=n; i++){
      if(dat[i] == node) printf( "%d ", i);
   }
}


int main()
{
   int n,m, p, c;
   cin >>n >>m;
   for(int i=0; i<m; i++)
   {
      cin >> p >> c;
      dat[c] = p;
   }

   int nodMax = FindNodOfMaxChildren(n);
   printf("%d\n%d\n", FindRoot(n), nodMax);
   PrintChildren(nodMax, n);
   return 0;
}