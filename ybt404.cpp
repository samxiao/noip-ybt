#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long

#define MAXN 1000
#define MAXW 1000
#define uint64_t unsigned long long

int main()
{
   int n;
   scanf("%d", &n);
   int x, y = 1;
   int cnt = 0;
   while (true)
   {
      int x2 = y * (y + 1) - 2 * n;
      if (x2 > 0 && x2 % 6 == 0)
      {
         x = x2 / 6;
         printf("%d %d\n", x, y);
         // if (++cnt > 2) break; // 可以有很多个答案. 短胡同 是否就是意味着只有一个？
      }
      y++;
   }

   return 0;
}
