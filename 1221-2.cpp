#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT(x) (sizeof(x) / sizeof(x[0]))
int a[100];
int g[100];

int n;
int gcd(int a, int b) {
   if (0 == a % b) return b;
   return gcd(b, a % b);
}

bool canAdd(int pos, int gNo) {
   bool canAdd = true;
   for (int k = 1; k <= n; k++) {
      if (g[k] == gNo) {
         if (1 == gcd(a[k], a[pos])) {
         } else {
            canAdd = false;
            break;
         }
      }
   }
   return canAdd;
}

int ans = 0;
void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      scanf("%d", &a[i]);
   }
   for (int i = 1; i <= n; i++) {
      if (!g[i]) {
         g[i] = i;
      }
      for (int gn = 1; gn <= i; gn++) {
         int gNo = g[gn];
         for (int j = i + 1; j <= n; j++) {
            if (!g[j])
               if (canAdd(j, gNo)) g[j] = gNo;
         }
      }
   }

   //sort(g + 1, g + n);  // error!
   sort(g + 1, g + n + 1);
   int pos = 1;
   while (!g[pos]) {
      pos++;
   }

   for (int i = pos; i <= n; i++) {
      if (g[i] != g[i - 1]) ans++;
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
