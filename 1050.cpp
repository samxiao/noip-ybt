#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int x, y ;
	double w, b;
	char c;
	scanf("%d",  &x);
	b = x / 3.0 + 50;
	w =  x / 1.2;
	printf("%s", w == b  ? "All" : (w > b ? "Bike" : "Walk"));

  return 0;
}
