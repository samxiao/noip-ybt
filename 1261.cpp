//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 1086
#define LIMIT_MAX 0x1ffffff
int n, ans, a[CAP_N][CAP_N], f[CAP_N], path[CAP_N], cnt;

void work1() {
   scanf("%d", &n);
   for (int i = 2; i <= n; i++) f[i] = LIMIT_MAX;
   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= n; j++) scanf("%d", &a[i][j]);
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j < i; j++) {
         if (a[j][i] && f[j] != LIMIT_MAX && f[i] > f[j] + a[j][i]) {
            f[i] = f[j] + a[j][i];
            path[i] = j;
         }
      }
   }
   printf("minlong=%d\n", f[n]);
   int x = n;
   stack<int> sp;
   sp.push(n);
   while (path[x]) sp.push(path[x]), x = path[x];
   while (sp.size()) printf("%d ", sp.top()), sp.pop();
}

int main() {
   work1();
   return 0;
}