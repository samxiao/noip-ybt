#include <bits/stdc++.h>
using namespace std;

int a[1000];
int ans;
void work1() {
   int n;
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      scanf("%d", &a[i]);
      ans += a[i];
   }
   ans /= n;
   for (int i = 1; i <= n; i++) {
      a[i] -= ans;
   }
   ans = 0;
   for (int i = 1; i <= n - 1; i++) {
      if (a[i]) {
         ans++;
         a[i + 1] += a[i];
      };
   }

   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}