//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 502  // 10 * 1000 * 1000 + 86

int dat[MAX_LEN];

//bool cmp(string x1, string x2) { return x1 < x2; }
void work1() {
   char s[1000];
   scanf("%s", &s);
   for (int i = 0; i < strlen(s); i++) {
      dat[s[i] - 'a']++;
   }

   int maxn = 0, ch = 0;
   for (int i = 0; i < 26; i++) {
      if (maxn < dat[i]) {
         maxn = dat[i];
         ch = i;
      }
   }
   printf("%c %d", ch + 'a', maxn);
}

int main() {
   work1();
   return 0;
}
