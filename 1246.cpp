//Sam.XIAO
#include <bits/stdc++.h>
using namespace std;

void work2() {
#define EPS 1e-20
   double L, w, c, L2;
   scanf("%lf%lf%lf", &L, &w, &c);
   double left = 0, right = acos(-1) / 2, mid;
   L2 = (1 + w * c) * L;
   int cnt = 0;
   //L'*sin(x) = L*x  => L'/L = x/sin(x)  //> 1
   while (right - left > EPS) {
      mid = (left + right) / 2;
      double newL2 = L * mid / sin(mid);
      if (abs(newL2 - L2) < 1e-12) break;
      if (newL2 > L2)
         right = mid;
      else
         left = mid;

      cnt++;
   }
   double r1, r2;
   r1 = L / 2 / sin(mid);
   r2 = L2 / 2 / mid;
   double ans1 = r1 * (1 - cos(mid));  // 3OK, 7WA
   double ans2 = r2 * (1 - cos(mid));  // AC
   //printf("%G,  %.10lf  ", mid, ans1);
   printf("%.3lf", ans2);
}

int main() {
   work2();
   return 0;
}
