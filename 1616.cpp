#include <bits/stdc++.h>
using namespace std;

#define S64 signed long long
int main() {
   S64 a, b, m, ans = 1;
   scanf("%lld%lld%lld", &a, &b, &m);
   a %= m;

   while (b) {
      if (b & 1) ans = (ans * a) % m;
      a = a * a % m;
      b /= 2;
   }
   printf("%lld", ans);
   return 0;
}