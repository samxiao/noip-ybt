//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}
#define N_MAX 987654321
#define N_MIN -N_MAX
int f1[286][102];
int f2[286][102];

void w1()
{
	int n, m, k, x, y, ans=0, cnt=0;
	scanf("%d%d", &n, &m);
 	for(int i=1; i<=n; i++){
		for(int j=1; j<=m; j++)
			scanf("%d", &f1[i][j]);
  }

 	for(int i = 1; i <= n; i++){
		for(int j = 1; j <= m; j++){
			int add4[][2]={{0,0}, {1,0},{-1,0},{0,-1},{0,1}};
			if(1 == i || 1 == j || n == i || j == m){
				f2[i][j] = f1[i][j];
			}
			else{
				for(int k=0; k<5; k++)
				 	f2[i][j] += f1[i+add4[k][0]][j+add4[k][1]];
				f2[i][j] = f2[i][j] / 5.0 + 0.5;
			}
		}
  }
  
 	for(int i=1; i<=n; i++){
		for(int j=1; j<=m; j++)
			printf("%d ", f2[i][j]);
		printf("\n");
  }

}

int main()
{
	w1();
  return 0;
}
