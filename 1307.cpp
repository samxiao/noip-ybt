//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N 10000
// 1307：【例1.3】高精度乘法
struct num_t {
   int x[10086];
   int len;
};

//高精度乘法 High precision multiplication
class hpm {
   num_t x1, x2, x3;
   num_t *a = &x1, *b = &x2, *c = &x3;
   void str2num(char *s, num_t *num) {
      int pos = 0;
      num->len = strlen(s);
      for (int i = strlen(s) - 1; i >= 0; i--)
         num->x[pos++] = s[i] - '0';
   }

  public:
   //   void getreult(void);
   void getreult(void) {
      int cnt = szCNT_OF(x3.x) - 1;
      while (!c->x[cnt]) {
         cnt--;
      }
      c->len = cnt+1;
      for (int i = c->len - 1; i >= 0; i--)
         printf("%d", c->x[i]);
   }

   hpm(char *a1, char *b1) {
      str2num(a1, a);
      str2num(b1, b);
   }

   void multi() {
      memset(&x3, 0, sizeof(x3));
      for (int i = 0; i < a->len; i++) {
         for (int j = 0; j < b->len; j++) {
            c->x[i + j] += a->x[i] * b->x[j];
         }
      }

      int cnt = szCNT_OF(x3.x);
      for (int i = 0; i < cnt - 1; i++) {
         c->x[i + 1] += c->x[i] / 10;
         c->x[i] %= 10;
      }

      getreult();
   }

   ~hpm() {}
};

char a[10086], b[10086];
void work1() {
   scanf("%s%s", a, b);
   hpm xy(a, b);
   xy.multi();
}

int main() {
   work1();
   return 0;
}
