//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N 10000

bool isOk(int *a, int len) {
   int i = 0, j = len - 1;
   while (i <= j) {
      if (a[i++] != a[j--]) return false;
   }
   return true;
}

void add(int *a, int *L, int base) {
   int len = *L;
   int *b = new int[len];
   for (int i = len - 1, j = 0; i >= 0; i--, j++) b[j] = a[i];

   int *c = new int[len + 6];
   memset(c, 0, sizeof(int) * (len + 6));

   for (int i = 0; i < len; i++) {
      c[i] += a[i] + b[i];
      if (c[i] >= base) c[i + 1]++, c[i] %= base;
   }
   if (c[len]) (*L)++;
   for (int i = 0; i < *L; i++) a[i] = c[i];
}

void work1() {
   char s1[10086];
   int n;
   scanf("%d%s", &n, s1);
   int a[10086], len;
   len = strlen(s1);
   for (int i = len - 1, j = 0; i >= 0; i--, j++) {
      if (isdigit(s1[i])) {
         a[j] = s1[i] - '0';
      } else {
         char ch = toupper(s1[i]);
         if ('A' <= ch && ch <= 'Z') a[j] = ch - 'A' + 10;
      }
   }
   int cnt = 0;
   while (!isOk(a, len)) {
      add(a, &len, n);
      cnt++;
      if (cnt > 30) break;
   }
   if (cnt > 30)
      printf("Impossible");
   else
      printf("%d", cnt);
}

int main() {
   work1();
   return 0;
}
