//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

long long f[MAX_LEN + 1][MAX_LEN + 1];

long long part(int n, int k) {
   if (1 == k || k == n) return 1;
   if (n < k || 0 == k) return 0;

   return part(n - 1, k - 1) + k * part(n - 1, k);
}

void work1() {
   int n, k;
   scanf("%d%d", &n, &k);

   printf("%lld", part(n, k));
}

int main() {
   work1();
   return 0;
}
