#include <bits/stdc++.h>

int a[33] = {0, 10, 20, 50, 100};
long long f[1033];

int main() {
   int m;
   scanf("%d", &m);
   f[0] = 1;
   for (int i = 1; i <= 4; i++) {
      for (int j = a[i]; j <= m; j++)
         f[j] += f[j - a[i]];
   }
   printf("%d", m == 0 ? m : f[m]);
}