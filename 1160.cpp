//SamXIAO
#include <bits/stdc++.h>
using namespace std;

void f(int x )
{
	printf("%d", x % 10);
	if(x<10) return;
	else f(x / 10);
}

void work1()
{
	int n;
	scanf("%d", &n);
	f(n);
}

int main()
{
	work1();
  return 0;
}
