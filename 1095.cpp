#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

long long f[100]={1,1,2,3};

void w1()
{
	int n, k, ans=0;
	scanf("%d", &n);
	for(int i=1; i<=n; i++){
		k = i;
		for(; k>0;){
			if(1 == k % 10) ans++;
			k /= 10;
		}
	}
	printf("%d", ans);
}

int main()
{
	w1();
  return 0;
}
