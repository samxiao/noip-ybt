//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86
// 第二部分 基础算法 --> 第六章 贪心算法 最后一题。
int w[1000085];
long long ans;

// luogu p1190. 注意m=1
void work1() {
   int n, m;
   scanf("%d%d", &n, &m);

   for (int i = 0; i < n; i++) {
      int x;
      scanf("%d", &x);
      w[i] = x;
   }

   if (n <= m) {
      sort(w, w + n);
      printf("%d", w[n - 1]);
      return;
   }

   //sort(w, w + n);
   int pos = m;
   while (true) {
      bool isAdd = false;
      for (int i = 0; i < m; i++) {
         if (w[i]) w[i]--, isAdd = true;
      }

      if (isAdd) ans++;
      for (int i = 0; i < m; i++) {
         if (0 == w[i]) {
            if (pos >= n) break;
            w[i] = w[pos++];
         }
      }

      bool isFishined = true;
      for (int i = 0; i < m; i++) {
         if (w[i]) {
            isFishined = false;
            break;
         }
      }
      if (isFishined) break;
   }
   printf("%lld\n", ans);
}

int main() {
   //freopen("P1190_10.in", "r", stdin);
   work1();
   return 0;
}
