//SamXIAO
#include <bits/stdc++.h>
using namespace std;


bool isprime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}
#define MAX_N 5000
int f[MAX_N]={2}, cnt = 1;
void gen()
{
	for(int i = 3; i < MAX_N; i += 2)
		if(isprime(i)) f[cnt++] = i;
}

int out[5000], oCnt = 0;

void judge(int x)
{
	for(int i = cnt - 1; i >= 0; i--){
		if(x < f[i]) continue;
		if(x % f[i] == 0){
			out[oCnt++] = f[i];
			break;
		}
	}
}

void work1()
{
	int m, n, pos=1, ans = 0;
	scanf("%d%d", &m, &n);
	gen();
	for(int i = m; i <= n; i++) judge(i);
	for(int i = 0; i < oCnt; i++)
    printf("%d%s", out[i], i < oCnt - 1 ? "," : "");
}

int main()
{
	work1();
  return 0;
}
