//SamXIAO
#include <bits/stdc++.h>
using namespace std;


bool isprime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}
#define MAX_N 100086
int f[MAX_N]={2}, cnt = 1;
void gen()
{
	for(int i = 3; i < MAX_N; i += 2)
		if(isprime(i)) f[cnt++] = i;
}

void work1()
{
	int m, n, pos=1, ans = 0;
	scanf("%d%d", &m, &n);
	gen();
	for(int i = 0; i < cnt; i++){
		if(m<= f[i] && f[i] <= n) ans++;
	}
	printf("%d", ans);
}

int main()
{
	work1();
  return 0;
}
