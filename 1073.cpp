#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

double ans=0;
int main()
{
	int n;
	scanf("%d", &n);
	int   z;
	double x, y;
	for(int i=0; i<n; i++){
		scanf("%lf%lf%d", &x, &y, &z);
		ans += sqrt(SQUARE(x) + SQUARE(y))/50.0*2 + 1.5*z;
 	}
 	printf("%d", (int)ceil(ans));
  return 0;
}
