#include <bits/stdc++.h>
using namespace std;

int a[102], ans;
//bool b[102], c[102], d[102];
bool f[102][102];
char ansTble[100086][30];
void makeFlag(int n, int x, int y, bool is1) {
   f[x][y] = is1;
   //b[y] = c[x + y] = d[x - y + 7] = is1;
}

bool getFlag(int n, int x, int y) {
   //return !f[x][y];
   // return (!b[y]) && (!c[x + y]) && (!d[x - y + 7]);

   int nx = x, ny = y;
   bool is1 = false;
   while (n >= nx && nx > 0 && n >= ny && ny > 0)
      if (f[nx++][ny++]) return false;
   nx = x, ny = y;
   while (n >= nx && nx > 0 && n >= ny && ny > 0)
      if (f[nx++][ny--]) return false;
   nx = x, ny = y;
   while (n >= nx && nx > 0 && n >= ny && ny > 0)
      if (f[nx--][ny++]) return false;
   nx = x, ny = y;
   while (n >= nx && nx > 0 && n >= ny && ny > 0)
      if (f[nx--][ny--]) return false;
   for (int i = 1; i <= n; i++)
      if (f[i][y] || f[x][i]) return false;

   return true;
}

void view(int n) {
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) printf("%d ", f[i][j]);
      printf("\n");
   }
   printf("\n");
}

void search(int n, int cnt) {
   int i;
   if (n < cnt) {
      ans++;
      int k = 0;
      for (int j = 1; j <= n; j++) {
         k += sprintf(ansTble[ans] + k, "%d", a[j]);
      }
      //printf("===%d\n", ans);
   }
   for (i = 1; i <= n; i++) {
      if (getFlag(n, cnt, i)) {
         a[cnt] = i;
         makeFlag(n, cnt, i, true);
         search(n, cnt + 1);
         makeFlag(n, cnt, i, false);
      }
   }
}

void work1() {
   int n, t;
   scanf("%d", &t);
   search(8, 1);
   while (t--) {
      scanf("%d", &n);
      printf("%s\n", ansTble[n]);
   }
}
int main() {
   work1();
   return 0;
}
