#include <iostream>
#include <bits/stdc++.h>

using namespace std;

#define szARR(x) sizeof(x) / sizeof(x[0])
#define DBG_PRINTF

 
int main()
{
	int m, w, s;
	cin >> m >> w >> s;
   int cntM=0, cntW=0;
	for(int i=0; i<s; i++){
      if(++cntM > m) cntM = 1;
      if(++cntW > m) cntW = 1;
      printf("%d %d\n", cntM, cntW);
   }
	return 0;
}
 