#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long
// P6033 合并果子 加强版 会TLE. 必须用手写堆+高精才能过.
#define MAXN 10002
#define MAXW 10002
int f[MAXW], w[MAXN], c[MAXN];
inline long long read()
{
   long long x = 0;
   char ch = getchar();
   while (!isdigit(ch))
   {
      ch = getchar();
   }
   while (isdigit(ch))
   {
      x = x * 10 + ch - '0';
      ch = getchar();
   }
   return x;
}

template <class T>
struct heap_t
{
#define HEAP_SIZE 1024 * 100
   int len;
   T dat[HEAP_SIZE];

   heap_t() { len = 0; }
   T top() { return dat[1]; }
   void push(T v)
   {
      dat[++len] = v;
      int x = len;
      while (x > 1)
      {
         if (dat[x] >= dat[x / 2])
            break;
         swap(dat[x], dat[x / 2]);
         x /= 2;
      }
   }

   T pop()
   {
      T r = dat[1];
      int x = 1;
      dat[x] = dat[len--];
      while (2 * x <= len)
      {
         int y = 2 * x;
         if (y + 1 <= len && dat[y + 1] < dat[y])
            y++;
         if (dat[y] >= dat[x])
            break;
         swap(dat[x], dat[y]);
         x = y;
      }
      return r;
   }

   int size() { return len; }
};

int main()
{
   int n, dat;
   U64_t ans = 0;
   heap_t<U64_t> h;
   //priority_queue<int, vector<int>, greater<int>> h; // little root heap
                                                     //   priority_queue<int, vector<int>, less<int> > h; // big root heap
   scanf("%d", &n);
   for (int i = 0; i < n; i++)
   {
      dat = read();
      h.push(dat);
   }
   while (true)
   {
      int x;
      if (1 == h.size())
         break;
      x = h.top(), h.pop();
      x += h.top(), h.pop();
      ans += x;
      h.push(x);
   }
   printf("%lld", ans);
   return 0;
}
