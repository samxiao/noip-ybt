#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

long long f[100]={1,1,2,3};

void w1()
{
	int n, k, ans=0, y;
	scanf("%d%d", &n, &y);
	for(int i=n; i<=y; i++){
		k = i;
		for(; k>0;){
			if(2 == k % 10) ans++;
			k /= 10;
		}
	}
	printf("%d", ans);
}

int main()
{
	w1();
  return 0;
}
