#include <iostream>
#include <bits/stdc++.h>

using namespace std;

#define szARR(x) sizeof(x) / sizeof(dat[0])
#define DBG_PRINTF
int dat[110][110];
struct xy_t
{
   int x, y;
   xy_t(int a, int b) { x = a, y = b; }
   xy_t(){}
};

int main()
{
   int n, m, ans = 0;
   cin >> n >> m;
   queue<xy_t> q;

   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= m; j++)
         cin >> dat[i][j];

   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= m; j++)
         if (1 == dat[i][j])
         {
            ans++;
            xy_t xy;
            q.push(xy_t(i,j));            
            //printf("%d %d/\n", i,j);
            while(!q.empty()){
               int x,y;
               xy=q.front();
               q.pop();
               x=xy.x, y=xy.y;
               dat[x][y] = 3;
               //printf("%d %d.  ", x, y);
               if(x>=0 && x<=n && y>=0 && y<=m){
                  if (1 == dat[x][y - 1])  q.push(xy_t(x, y - 1));
                  if (1 == dat[x][y + 1])  q.push(xy_t(x, y + 1));
                  if (1 == dat[x-1][y])    q.push(xy_t(x-1, y));
                  if (1 == dat[x+1][y])    q.push(xy_t(x+1, y));
               }
            }
         }
   cout << ans;
   return 0;
}
