#include <bits/stdc++.h>
using namespace std;

int w[1002], h[1002], f[1002][10086];
char b[1002][10086];
int main()
{
   int n, s;

   scanf("%d%d", &n, &s);
   for (int i = 1; i <= n; i++)
      scanf("%d%d", &w[i], &h[i]);
   for (int i = 1; i <= n; i++)
   {
      for (int j = 0; j <= s; j++)
      {
         if (j >= w[i])
         {
            if (f[i][j] < f[i - 1][j - w[i]] + h[i])
            {
               f[i][j] = f[i - 1][j - w[i]] + h[i];
               b[i][j] = 1;
            }
            //  f[i][j] = max(f[i][j], f[i - 1][j - w[i]] + h[i]);
         }
         else
            f[i][j] = f[i - 1][j];
      }
   }
   printf("%d\n", f[n][s]);
   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= s; j++)
         if (b[i][j])
            printf("%d ", i);
   return 0;
}

/*

3 10 
10 9 
1 7 
9 4

*/