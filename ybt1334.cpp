#include <iostream>
#include <bits/stdc++.h>

using namespace std;

#define szARR(x) sizeof(x) / sizeof(x[0])
#define DBG_PRINTF

int main()
{
   int n, m, s;
   cin >> n >> m;
   int cnt = 0;
   queue<int> q;
   for (int i = 1; i <= n; i++)
      q.push(i);
   while (!q.empty())
   {
      if (++cnt >= m)
      {
         cnt = 0;
         printf("%d ", q.front());
         q.pop();
      }
      else
      {
         q.push(q.front());
         q.pop();
      }
   }
   return 0;
}
