//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, m, ans = -999991234, f[CAP_N][CAP_N], dat[CAP_N][CAP_N];

int one(int n, int m) {
   int res = 0;

   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
         f[i][j] = max(f[i - 1][j], f[i][j - 1]) + dat[i][j];
      }
   }
   printf("%d\n", f[n][m]);

   return res;
}

void work1() {
   int t;
   scanf("%d", &t);
   while (t--) {
      memset(f, 0, sizeof(f));
      memset(dat, 0, sizeof(dat));

      scanf("%d%d", &n, &m);
      for (int i = 1; i <= n; i++) {
         for (int j = 1; j <= m; j++) {
            scanf("%d", &dat[i][j]);
         }
      }
      one(n, m);
   }
}

int main() {
   work1();
   return 0;
}
