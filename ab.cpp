#include <bits/stdc++.h>
using namespace std;

#if 0 //#ifndef ONLINE_JUDGE
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define MAXN 200000
/*
SamXIAO 200126

RE. 65  99877
32

8179/26345    8179/26346    8180/26346    8181/26346    
8181/26347    8181/26348    8181/26349    8182/26349    
8182/26350    8183/26350    8183/26351    8183/26352    
8184/26352    8185/26352    8185/26353    8185/26354    
8186/26354    8186/26355    8187/26355    8187/26356 
...
7682/29996    7682/29997    7683/29997    7684/29997    
7684/29998    7684/29999    7684/30000    7685/30000    
7685/30001    7685/30002    7686/30002
*/

int qdat[MAXN];
struct qu_t
{
   int head, tail;
   qu_t()
   {
      head = tail = 0;
   }
   void push(int x)
   {
      if (full())
         printf("xxxxx FULL xxxxx ");
      qdat[tail++] = x;
      //printf(" %d.", x);
   }
   void pop()
   {
      head++;
   }
   int front()
   {
      if (empty())
         printf("EMPTY2.. ");
      return qdat[head];
   }
   bool empty()
   {
      return head == tail;
   }
   bool full()
   {
      return tail == MAXN - 3;
   }
};
qu_t q;
//queue<int> q2; // 7686 RE
int f[MAXN] = {0};
int cnt = 0;

void nxt(int e2, int stp)
{
   try
   {
//   if (e2 > 0 && !f[e2] && e2 < MAXN) // ERROR //!f[e2]会执行，但越界了从而出错
//      if (!f[e2] && e2 > 0 && e2 < MAXN) // ERROR//!f[e2]会执行，但越界了从而出错
#if 1
      if (e2 > 0 && e2 < MAXN && !f[e2]) // ok!  //e2 < MAXN对f[e2]有短路作用，!f[e2]不会执行，从而不会出错
      {
#else
      if (e2 > 0 && e2 < MAXN)
      {
         if (!f[e2])
#endif
         {
            f[e2] = stp;
            q.push(e2);
         }
      }
      //else printf("  ...%d...", e2);
   }
   catch (std::exception &e)
   {
      printf("  .....exception....");
      //其他的错误
   }
}

int main()
{
   int a, b, ans = 0;
   cin >> a >> b;
   q.push(a);
   f[a] = 1;
   while (true)
   {
      cnt++;
      int e1;
      if (q.empty())
      {
         printf(" EMPTY @%d\n", cnt);
         break;
      }
      e1 = q.front();
      q.pop();
      if (e1 == b)
      {
         ans = f[e1];
         break;
      }
      int stp = f[e1] + 1;
      nxt(e1 * 2, stp);
      nxt(e1 + 1, stp);
      nxt(e1 - 1, stp);
   }

   printf("%d @%d", ans, cnt);
   return 0;
}

/*
 
*/
