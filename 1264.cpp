//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 1086
int n, ans, a[CAP_N], fu[CAP_N], fd[CAP_N];

void work1() {
   scanf("%d", &n);
   for (int i = 0; i < n; i++) scanf("%d", &a[i]);
   for (int i = 0; i < n; i++) {
      fu[i] = 1;
      for (int j = 0; j < i; j++)
         if (a[i] > a[j]) fu[i] = max(fu[i], fu[j] + 1);
   }
   for (int i = n - 1; i >= 0; i--) {
      fd[i] = 1;
      for (int j = n - 1; j > i; j--) {
         if (a[i] > a[j]) fd[i] = max(fd[i], fd[j] + 1);
      }
   }
   for (int i = 0; i < n; i++) {
      ans = max(ans, fd[i] + fu[i] - 1);
   }

   printf("%d", n - ans);
}

int main() {
   work1();
   return 0;
}