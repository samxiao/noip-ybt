#include <bits/stdc++.h>
using namespace std;

int dat[100096];
bool cmp(int x1, int x2) { return x1 > x2; }
int main() {
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      scanf("%d", &dat[i]);
   }
   sort(dat, dat + n, cmp);
   int k;
   scanf("%d", &k);

   for (int i = 0; i < k; i++) {
      printf("%d\n", dat[i]);
   }
   return 0;
}
