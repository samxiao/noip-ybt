#include <bits/stdc++.h>
using namespace std;

int v[10086], t[10086];

void work1() {
   int n;
   while (true) {
      scanf("%d", &n);
      if (0 == n) break;
      for (int i = 1; i <= n; i++) {
         scanf("%d%d", &v[i], &t[i]);
      }
      double l = 0, ans = 1e33;
      for (int i = 1; i <= n; i++) {
         if (t[i] >= 0) {
            double t2 = 4500.0 / (v[i] / 3.6) + t[i];
            ans = min(ans, t2);
         }
      }

      printf("%d\n", (int)ceil(ans));
   }
}

int main() {
   work1();
   return 0;
}
