#include <bits/stdc++.h>
using namespace std;

long long ans = 0;

long long f[100086];
void gen() {
   f[1] = 1, f[2] = 2, f[3] = 4;
   for (int i = 4; i <= 71; ++i) {
      f[i] = f[i - 3] + f[i - 2] + f[i - 1];
   }
}
int main() {
   int n;
   gen();
   scanf("%d", &n);
   while (n) {
      printf("%lld\n", f[n]);
      scanf("%d", &n);
   }

   return 0;
}
