//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 1086
int n, ans, a[CAP_N], f[CAP_N];

void work1() {
   scanf("%d", &n);
   for (int i = 0; i < n; i++) scanf("%d", &a[i]);
   for (int i = 0; i < n; i++) {
      f[i] = 1;
      for (int j = 0; j < i; j++) {
         if (a[i] <= a[j]) f[i] = max(f[i], f[j] + 1);
      }
      ans = max(ans, f[i]);
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}