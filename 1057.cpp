#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int x, y, z, ans;
	double x2, b;
	char c;
	scanf("%d%d %c",  &x, &y, &c);
	switch(c)
	{
		case '+': ans = x + y; break;
		case '-': ans = x - y; break;
		case '*': ans = x * y; break;
		case '/':
			if(0 == y) {
				printf("Divided by zero!");
				return 0;
			}
			ans = x / y;
			break;
		default:
			printf("Invalid operator!");
			return 0;
	}
	printf("%d", ans);

  return 0;
}
