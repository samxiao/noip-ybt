//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif

char a[108][106];
int res[108][108];
struct xy_t {
   int x, y;
   xy_t(int x, int y) : x(x), y(y) {}
};

queue<xy_t> q;
int ans, n, m, step;

bool isIn(int x, int y) {
   return 1 <= x && 1 <= y && x <= n && y <= m;
}

void bfs(int x, int y) {
   if ('.' != a[x][y]) return;
   a[x][y] = 'x';
   step++;
   res[x][y] = step;
   q.push(xy_t(x, y));
   while (!q.empty()) {
      xy_t rm = q.front();
      q.pop();
      x = rm.x, y = rm.y;
      DBG_PRINT("%d,%d ", x, y);
      int dxy[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
      for (int i = 0; i < szARR_CNT_OF(dxy); i++) {
         int nx, ny;
         nx = x + dxy[i][0], ny = y + dxy[i][1];
         if (isIn(nx, ny) && '.' == a[nx][ny]) {
            a[nx][ny] = 'x';
            res[nx][ny] = res[x][y] + 1;
            q.push(xy_t(nx, ny));
         }
      }
   }
   DBG_PRINT("\n");
}

void work1() {
   scanf("%d%d", &n, &m);
   int x1, y1, x2, y2;
   for (int i = 1; i <= n; i++) scanf("%s", &a[i][1]);
   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= m; j++) {
         if ('S' == a[i][j]) x1 = i, y1 = j;
         if ('T' == a[i][j]) x2 = i, y2 = j;
      }
   a[x1][y1] = a[x2][y2] = '.';
   bfs(x1, y1);

   printf("%d", res[x2][y2] - 1);
}

int main() {
   work1();
   return 0;
}