//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char s1[1000];
int f(int n, int k)
{
	if(1 == k) return n % 10;
	if(0 == n) return 0;
	return f(n / 10, k - 1);
}

void work1()
{
	int n, k;
	scanf("%d%d", &n, &k);
	printf("%d", f(n, k));
}

int main()
{
	work1();
  return 0;
}
