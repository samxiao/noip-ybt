//SamXIAO
#include <bits/stdc++.h>
using namespace std;


bool isprime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}
#define MAX_N 100086
int f[MAX_N]={2}, cnt = 1;
void gen()
{
	for(int i = 3; i < MAX_N; i += 2)
		if(isprime(i)) f[cnt++] = i;
}

int out[MAX_N], oCnt = 0;

void judge(int x)
{
	int ret = x;
	if(isprime(x)){
		int x2 = 0;
		while(x){
			x2 *= 10;
			x2 += x % 10;
			x /= 10;
		}
		if(isprime(x2)) out[oCnt++] = ret;
	}
}

void work1()
{
	int m, n, pos=1, ans = 0;
	scanf("%d%d", &m, &n);
	gen();
	for(int i = m; i <= n; i++) judge(i);
	for(int i = 0; i < oCnt; i++)
    printf("%d%s", out[i], i < oCnt - 1 ? "," : "");
	if(oCnt == 0) printf("No");
}

int main()
{
	work1();
  return 0;
}
