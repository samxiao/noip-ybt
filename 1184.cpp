//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 5500  // 10 * 1000 * 1000 + 86

int dat[MAX_LEN];

void work1() {
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      int x;
      scanf("%d", &x);
      dat[x] = x;
   }
   sort(dat, dat + MAX_LEN);
   int cnt = 0;
   for (int i = 0; i < MAX_LEN; i++) {
      if (dat[i]) cnt++;
   }
   printf("%d\n", cnt);
   for (int i = 0; i < MAX_LEN; i++) {
      if (dat[i]) printf("%d ", dat[i]);
   }
}

int main() {
   work1();
   return 0;
}
