//SamXIAO
#include <bits/stdc++.h>
using namespace std;

bool isPrime(int x)
{
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}

inline int revers(int x)
{
	return (x % 10) * 10 + x / 10;
}
void w1()
{
	for(int i = 10; i < 99; i++)
		if(isPrime(i) && isPrime(revers(i)) ) printf("%d\n", i);
}

int main()
{
	w1();
  return 0;
}
