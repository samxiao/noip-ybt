//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, ans = -999991234, f[CAP_N][CAP_N], dat[CAP_N][CAP_N];

void work1() {
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
         scanf("%d", &dat[i][j]);
      }
   }

   for (int i = 1; i <= n; i++) {
      f[i][1] = f[i - 1][1] + dat[i][1];
   }

   DBG_PRINT("\n");
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
         f[i][j] = f[i][j - 1] + f[i - 1][j] - f[i - 1][j - 1] + dat[i][j];
         DBG_PRINT("%3d ", f[i][j]);
      }
      DBG_PRINT("\n");
   }
   for (int x1 = 1; x1 <= n; x1++) {
      for (int y1 = 1; y1 <= n; y1++) {
         for (int x2 = x1; x2 <= n; x2++) {
            for (int y2 = y1; y2 <= n; y2++) {
               ans = max(ans, f[x2][y2] - f[x2][y1 - 1] - f[x1 - 1][y2] + f[x1 - 1][y1 - 1]);
            }
         }
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
