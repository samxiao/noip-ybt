//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char s1[1000];
double f(int n, double x)
{
	if(0 == n) return 1;
	if(1 == n) return 2*x;
	else if(1 < n) return 2 * x * f(n-1, x) - 2*(n-1)*f(n-2, x);
}

void work1()
{
	int n, k;
	scanf("%d%d", &n, &k);
	printf("%.2lf", f(n, k));
}

int main()
{
	work1();
  return 0;
}
