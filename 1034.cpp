#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	double x[3],y[3], z[3], p;
	char c1,c2,c3;
  int h,r;
	float f;
	for(int i=0; i<3; i++) scanf("%lf%lf",  &x[i], &y[i]);
	for(int i=0; i<2; i++) z[i] = sqrt(SQUARE(x[i] - x[i+1]) + SQUARE(y[i] - y[i+1]));
	z[2] = sqrt(SQUARE(x[2] - x[0]) + SQUARE(y[2] - y[0]));
	p = (z[0] + z[1] + z[2]) / 2.0;
  printf("%.2lf", sqrt(p*(p-z[0])*(p-z[1])*(p-z[2])));

  return 0;
}
