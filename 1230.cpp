//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

bool f[102];
struct point {
   int x, y;
} a[102];

bool cmp(point x1, point x2) {
   if (x1.x == x2.x) return x1.y < x2.y;
   return x1.x < x2.x;
}

void work1() {
   int n;
   scanf("%d", &n);

   for (int i = 0; i < n; i++) {
      int x, y;
      scanf("%d%d", &x, &y);
      a[i].x = x;
      a[i].y = y;
   }
   sort(a, a + n, cmp);

   for (int i = 0; i < n; i++) {
      f[i] = true;
      for (int j = i + 1; j < n; j++) {
         if (a[i].x <= a[j].x && a[i].y <= a[j].y) {
            f[i] = false;
            break;
         }
      }
   }
   for (int i = 0; i < n; i++) {
      if (f[i]) printf("(%d,%d)%s", a[i].x, a[i].y, i == n - 1 ? "" : ",");
   }
}

int main() {
   work1();
   return 0;
}
