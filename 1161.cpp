//SamXIAO
#include <bits/stdc++.h>
using namespace std;

void f(int x, int b)
{
	
	if(x == 0 ) return;
	else f(x / b, b);
	printf("%X", x % b);
}

void work1()
{
	int n, b;
	scanf("%d%d", &n, &b);
	if(n) f(n, b);
	else 	printf("0");
}

int main()
{
	work1();
  return 0;
}
