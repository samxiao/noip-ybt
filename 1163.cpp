//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char s1[1000];
int f(int m, int n)
{
		if(0 == m ) return n + 1;
		else if(0 < m && 0 == n) return f(m - 1, 1);
		else if(0 < m && 0 < n)  return f(m - 1, f(m, n - 1));
}

void work1()
{
	int n, m;
	scanf("%d%d", &m, &n);
	printf("%d", f(m, n));
}

int main()
{
	work1();
  return 0;
}
