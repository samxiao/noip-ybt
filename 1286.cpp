//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define CAP_N 103
#define LIMIT_MAX 0x1ffffff
int n, m, ans = -999991234, f[CAP_N], dat[CAP_N];

int LIS(int n) {
   int res = 0;

   memset(f, 0, sizeof(f));
   for (int i = 1; i <= n; i++) {
      f[i] = 1;
      for (int j = 1; j < i; j++) {
         if (dat[i] < dat[j]) f[i] = max(f[i], f[j] + 1);
      }
      res = max(f[i], res);
   }

   return res;
}

int LDS(int n) {
   int res = 0;
   memset(f, 0, sizeof(f));

   for (int i = 1; i <= n; i++) {
      f[i] = 1;
      for (int j = 1; j < i; j++) {
         if (dat[i] > dat[j]) f[i] = max(f[i], f[j] + 1);
      }
      res = max(f[i], res);
   }

   return res;
}

void work1() {
   int t;
   scanf("%d", &t);
   while (t--) {
      memset(dat, 0, sizeof(dat));

      scanf("%d", &n);
      for (int i = 1; i <= n; i++) scanf("%d", &dat[i]);
      int x = LIS(n), y = LDS(n);
      printf("%d\n", max(x, y));
   }
}

int main() {
   work1();
   return 0;
}
