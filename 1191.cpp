//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 102  // 1 * 1000 * 1000 + 86
#define MODN 32767

char f[MAX_LEN][MAX_LEN];
char f2[MAX_LEN][MAX_LEN];

void pp(int n) {
   return;
   for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
         printf("%c", f[i][j]);
      }
      printf("\n");
   }
   printf("\n");
}
void lg(int n) {
   for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
         if ('@' == f[i][j]) {
            int xy[][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
            //int xy[][2] = {{0, 1}, {0, -1}};
            for (int k = 0; k < szCNT_OF(xy); k++) {
               int dx = i + xy[k][0], dy = j + xy[k][1];
               if (0 <= dx && dx < n && 0 <= dy && dy < n && '.' == f[dx][dy])
                  f2[dx][dy] = '@';
            }
         };
      }
   }

   memcpy(f, f2, sizeof(f2));
   pp(n);
}

void work1() {
   int n;
   scanf("%d", &n);

   for (int i = 0; i < n; i++) {
      char ch[105];
      scanf("%s", ch);
      for (int j = 0; j < strlen(ch); j++) {
         f[i][j] = ch[j];
      }
   }
   int m, ans = 0;
   scanf("%d", &m);
   memcpy(f2, f, sizeof(f2));
   for (int i = 1; i < m; i++) lg(n);
   for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
         if ('@' == f[i][j]) ans++;
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
