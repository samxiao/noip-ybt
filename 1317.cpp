#include <bits/stdc++.h>
using namespace std;

void combination(int n, int k, int stp) {
   static int a[5 * 1000 * 1000] = {1};
   static bool vi[100] = {0};

   if (stp == k + 1) {
      for (int j = 1; j <= k; j++) {
         printf("%3d", a[j]);
      }
      printf("\n");
   }

   //for (int i = 1; i <= n; i++) { // permutation
   for (int i = a[stp - 1]; i <= n; i++) {  // combination
      if (!vi[i]) {
         vi[i] = true;
         a[stp] = i;
         combination(n, k, stp + 1);
         vi[i] = false;
      }
   }
}

void work1() {
   int n, k;
   scanf("%d%d", &n, &k);
   combination(n, k, 1);
}
int main() {
   work1();
   return 0;
}
