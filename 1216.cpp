#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT(x) (sizeof(x) / sizeof(x[0]))
char a[102][102];
int ans;
bool f[102][102];

int w, h;
int frX, frY;
int dxy[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
void search(int x, int y) {
   if (!(0 <= x && x < h && 0 <= y && y < w)) return;
   if ('.' == a[x][y] || '@' == a[x][y]) {
      ans++;
      a[x][y] = ';';
      for (int i = 0; i < szARR_CNT(dxy); i++) {
         int nx = x + dxy[i][0], ny = y + dxy[i][1];
         search(nx, ny);
      }
   }
}

void work1() {
   while (true) {
      scanf("%d%d", &w, &h);
      if (0 == w && 0 == h) break;
      for (int i = 0; i < h; i++) {
         scanf("%s", a[i]);
         for (int j = 0; j < w; j++) {
            if ('@' == a[i][j]) frX = i, frY = j;
         }
      }
      ans = 0;
      search(frX, frY);
      printf("%d\n", ans);
      memset(a, 0, sizeof(a));
   }
}
int main() {
   work1();
   return 0;
}
