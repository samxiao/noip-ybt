//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAXLEN 5000

int n, dat[10 * 1000 * 1000 + 86];
int ans;
void work1() {
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      scanf("%d", &dat[i]);
   }
   for (int i = 0; i < n; i++) {
      for (int j = i + 1; j < n; j++) {
         if(dat[i] > dat[j]) swap(dat[i], dat[j]), ans++;
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}
