#include <bits/stdc++.h>

using namespace std;

#define int64_t signed long long
#define uint64_t unsigned long long

struct ele_t {
   bool isOp;
   union n__ {
      double num;
      char op;
   } n;
};

ele_t in[1000];
double num[1000];
int topIn, topN;

/*
- 3 2


input:
* + 11.0 12.0 + 24.0 35.0
output: 
1357.000000
=======================================
结尾的33 12 39 是多余的
input:
* + 11.0 12.0 + 24.0 35.0  33 12 39
output: 
1357.000000
=======================================
结尾的+ + *是多余的
input:
* + 11.0 12.0 + 24.0 35.0 + + *
output: 
1357.000000
=======================================
结尾的888 999 101 + + *是多余的
input:
* + 11.0 12.0 + 24.0 35.0 888 101 + + *
output: 
1357.000000
=======================================
结尾的888 + / 101是多余的
input:
* + 11.0 12.0 + 24.0 35.0 888 + / 101 + + *
* + 11.0 12.0 + 24.0 35.0 888 + / 101
结尾的888 + 101是多余的
*  +  11.0  12.0 +  24.0  35.0  +  101
开头的3多余
3 *  +  11.0  12.0 +  24.0  35.0  +  101

output: 
1357.000000
=======================================
如果用递归就不会正理结尾的多余数字或运算符。
*/
double calc() {
   double x1,x2;
   bool numIsDetected = false;
   while (topIn) {
      if (in[topIn].isOp) {
         //if(1 == topN) return num[topN];
         char c = in[topIn--].n.op;
         // if(!numIsDetected) continue;
         x1 = num[topN--];
         x2 = num[topN];
         switch(c)
         {
            case '+': num[topN] = x1 + x2 ;break;
            case '-': num[topN] = x1 - x2 ;break;
            case '*': num[topN] = x1 * x2 ;break;
            case '/': num[topN] = x1 / x2 ;break;
         }
      } else {
         numIsDetected = true;
         num[++topN] = in[topIn--].n.num;
      }
   }
   return num[topN--];
}

void work_v1() {
   char s[1000];
   int cntOp = 0, cntNum = 0;
   bool isOpDectected = false;
   while (~scanf("%s", &s)) {
      if (isdigit(s[0])) {
         //if(!isOpDectected) continue;
         in[++topIn].isOp = false;
         in[topIn].n.num = atof(s);
         if(++cntNum > cntOp) break;
      } else if ('+' == s[0] || '-' == s[0] || '*' == s[0] || '/' == s[0]) {
         isOpDectected = true;
         cntOp++;
         in[++topIn].isOp = true;
         in[topIn].n.op = s[0];
      }
   }
   printf("%f", calc());
}

int main() {
   work_v1();
   return 0;
}
