#include <bits/stdc++.h>
using namespace std;

int a[102][102];
int f[102][102];

void work1() {
   int n;
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
         scanf("%d", &a[i][j]);
         f[i][j] = f[i - 1][j] + f[i][j - 1] - f[i - 1][j - 1] + a[i][j];
      }
   }

   int ans = -99999999;
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
         for (int i2 = i; i2 <= n; i2++) {
            for (int j2 = j; j2 <= n; j2++) {
               ans = max(f[i2][j2] - f[i - 1][j2] - f[i2][j - 1] + f[i - 1][j - 1], ans);
            }
         }
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}