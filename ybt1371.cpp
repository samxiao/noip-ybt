#include <bits/stdc++.h>
using namespace std;

struct bl_t
{
   char op[20];
   char name[30];
   int pri;
   friend bool operator<(bl_t d1, bl_t d2)
   {
      return d1.pri < d2.pri;
   }
};

void work_v1()  // 2WA, 2TLE, 6OK
{
   priority_queue<bl_t> h;
   bl_t op;
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; ++i)
   {
      string oneLine;
      getline(cin, oneLine); // cin , scanf混用，有的会少读一行。
      int cntIn = sscanf(oneLine.c_str(), "%s%s%d", op.op, op.name, &op.pri);
      if (1 == cntIn)
      {
         if (h.size())
         {
            bl_t x = h.top();
            h.pop();
            printf("%s %d\n", x.name, x.pri);
         }
         else
            printf("none\n");
      }
      else if (3 == cntIn)
      {
         h.push(op);
      }
   }
}


int GetLine(char *p){
   char ch=getchar();
   while(!isprint(ch)) ch = getchar();
   int i=0;
   while(isprint(ch)) p[i++] = ch, ch=getchar();
   return i;
}

void work_v2 () //AC.
{
   priority_queue<bl_t> h;
   bl_t op;
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; ++i)
   {
      char oneLine[100];
      memset(oneLine, 0, sizeof(oneLine));
      GetLine(oneLine);
      int cntIn = sscanf(oneLine, "%s%s%d", op.op, op.name, &op.pri);
      if (1 == cntIn)
      {
         if (h.size())
         {
            bl_t x = h.top();
            h.pop();
            printf("%s %d\n", x.name, x.pri);
         }
         else
            printf("none\n");
      }
      else if (3 == cntIn)
      {
         h.push(op);
      }
   }
}

int main()
{
   work_v2();
   return 0;
}
