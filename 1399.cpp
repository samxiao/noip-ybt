//SamXIAO
#include <bits/stdc++.h>
using namespace std;

void w1()
{
	int n, ans=0;
	scanf("%d", &n);
	while(n--){
		int   y;
		float x;
		char name[100];
		memset(name, 0, sizeof(name));
		scanf("%s%f%d", name, &x, &y);
		if(x >= 37.5 && y) printf("%s\n", name), ans++;
	}
	printf("%d", ans);
}

int main()
{
	w1();
  return 0;
}
