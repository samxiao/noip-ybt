#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

long long f[100]={1,1,2,3};

void w1()
{
	int n, k, ans=0;
	scanf("%d", &n);
	for(int i=1; i<=n; i++){
		k = i;
		bool is7 = k % 7 == 0;
		for(; k>0 && !is7 ;){
			if(7 == k % 10) is7 = true;
			k /= 10;
		}
		if(!is7) ans += i*i;
	}
	printf("%d", ans);
}

int main()
{
	w1();
  return 0;
}
