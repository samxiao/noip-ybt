//Sam.XIAO
#include <bits/stdc++.h>
using namespace std;

int ans;
int dat[10086], tmp[10086];
template <class T>
void msort(T *dat, int l, int r) {
   if (l >= r) return;
   int mid = l + (r - l) / 2;
   msort(dat, l, mid);
   msort(dat, mid + 1, r);
   int i = l, j = mid + 1, k = l;
   while (i <= mid && j <= r) {
      if (dat[i] <= dat[j])
         tmp[k++] = dat[i++];
      else {
         ans += mid - i + 1;
         tmp[k++] = dat[j++];
      }
   }
   while (i <= mid) tmp[k++] = dat[i++];
   while (j <= r) tmp[k++] = dat[j++];
   for (int i = l; i <= r; i++) dat[i] = tmp[i];
}

void work2() {
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) scanf("%d", &dat[i]);

   msort(dat, 0, n - 1);
   //for (int i = 0; i < n; i++) printf("%d%s", dat[i], i == n - 1 ? "\n" : " ");
   printf("%d", ans);
}

int main() {
   work2();
   return 0;
}
