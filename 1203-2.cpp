//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

char ch[1000];

int dg(int n) {
   if (-1 == n) return -1;
   if (ch[n] == '$') return n;
   return dg(n - 1);
}
void work1() {
   while (scanf("%s", ch) != EOF) {
      printf("%s\n", ch);

      for (int i = 0; i < strlen(ch); i++) {
         if ('(' == ch[i])
            ch[i] = '$';
         else if (')' == ch[i]) {
            int pos = dg(i - 1);
            if (-1 == pos)
               ch[i] = '?';
            else if (pos >= 0)
               ch[pos] = ' ', ch[i] = ' ';
         } else
            ch[i] = ' ';
      }
      printf("%s\n", ch);
      memset(ch, 0, sizeof(ch));
   }
}

int main() {
   work1();
   return 0;
}
