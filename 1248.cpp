//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define XY_N 102

struct xy_t {
   int x, y, z;
   xy_t() {}
   xy_t(int x, int y, int z) : x(x), y(y), z(z) {}
};
int z, n, m, ans[XY_N][XY_N][XY_N];
char a[XY_N][XY_N][XY_N];
queue<xy_t> q;

void init() {
   memset(a, 0, sizeof(a));
   memset(ans, 0, sizeof(ans));
}

inline bool isIn(xy_t p) {
   return 0 <= p.x && p.x < n && 0 <= p.y && p.y < m && 0 <= p.z && p.z < z;
}
void bfs(xy_t fr) {
   if (!isIn(fr) || '.' != a[fr.z][fr.x][fr.y]) return;
   q.push(fr);
   a[fr.z][fr.x][fr.y] = '#';
   while (!q.empty()) {
      xy_t p = q.front();
      q.pop();
      int dxy[][3] = {{0, 0, 1}, {0, 0, -1}, {0, 1, 0}, {0, -1, 0}, {1, 0, 0}, {-1, 0, 0}};
      for (int i = 0; i < szARR_CNT_OF(dxy); i++) {
         int nx = p.x + dxy[i][1], ny = p.y + dxy[i][2], nz = p.z + dxy[i][0];
         if (isIn(xy_t(nx, ny, nz)) && '.' == a[nz][nx][ny]) {
            a[nz][nx][ny] = '#';
            ans[nz][nx][ny] = ans[p.z][p.x][p.y] + 1;
            q.push(xy_t(nx, ny, nz));
         }
      }
   }
}

void work1() {
   int t;
   while (true) {
      init();
      xy_t xfr, xto;
      scanf("%d%d%d", &z, &n, &m);
      if (0 == z && 0 == n && 0 == m) break;
      for (int i = 0; i < z; i++)
         for (int j = 0; j < n; j++) scanf("%s", a[i][j]);

      for (int i = 0; i < z; i++)
         for (int j = 0; j < n; j++)
            for (int k = 0; k < m; k++) {
               if ('S' == a[i][j][k]) xfr = xy_t(j, k, i);
               if ('E' == a[i][j][k]) xto = xy_t(j, k, i);
            }
      a[xfr.z][xfr.x][xfr.y] = '.';
      a[xto.z][xto.x][xto.y] = '.';
      bfs(xfr);
      int x = ans[xto.z][xto.x][xto.y];
      x ? printf("Escaped in %d minute(s).\n", x) : printf("Trapped!\n");
   }
}

int main() {
   work1();
   return 0;
}