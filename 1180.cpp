//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 5500  // 10 * 1000 * 1000 + 86

struct stu_t {
   int no, f;
   stu_t() {}
   stu_t(int no, int f) : no(no), f(f) {}
   bool operator<(stu_t const b) {
      if (f == b.f) return no < b.no;
      return f > b.f;
   }
};

stu_t dat[MAX_LEN];
bool cmp(stu_t x1, stu_t x2) { return x1 < x2; }
void work1() {
   int n, m;
   int t = 0;
   scanf("%d%d", &n, &m);
   for (int i = 0; i < n; i++) {
      int w, s, e;
      scanf("%d%d", &w, &s);
      dat[i] = stu_t(w, s);
      t += s;
   }
   m = floor(m * 1.5);
   sort(dat, dat + n, cmp);
   int pos = 0;
   if (m >= n) m = n;
   int cnt = 0;
   while (dat[pos].f >= dat[m - 1].f && pos < n) {
      cnt++;
      pos++;
   }
   printf("%d %d\n", dat[m - 1].f, cnt);
   for (int i = 0; i < cnt; i++) {
      printf("%d %d\n", dat[i].no, dat[i].f);
   }
}

int main() {
   work1();
   return 0;
}
