//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

int a[10086];
int f[10086][102];  //f[i][j]:=前i个数模k的值等不等于j

void work1() {
   int n, k;
   scanf("%d%d", &n, &k);
   for (int i = 1; i <= n; i++) {
      scanf("%d", a + i);
      a[i] = (a[i] % k + k) % k;
   }
   f[0][0] = 1;
   for (int i = 1; i <= n; i++)
      for (int j = 0; j < k; j++)
         f[i][j] = f[i - 1][(j + k - a[i]) % k] ||
                   f[i - 1][(j + a[i]) % k];
   if (f[n][0])
      printf("YES");
   else
      printf("NO");
}

int main() {
   work1();
   return 0;
}
