//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char a[10008][10086];
int ans, n = 100;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
// unformat
int dxy[][2] = {
    {2, 1},
    {2, -1},
    {-2, 1},
    {-2, -1},
    {1, 2},
    {1, -2},
    {-1, 2},
    {-1, -2},
    {2, 2},
    {2, -2},
    {-2, 2},
    {-2, -2},
};

struct xy_t {
   int x, y;
   xy_t(int x, int y) : x(x), y(y) {}
};
queue<xy_t> q;

void work1() {
   int x1, y1, x2, y2;
   scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
   int step = 1;

   q.push(xy_t(1, 1));
   a[1][1] = step;

   while (q.size()) {
      xy_t xy = q.front();
      q.pop();
      for (int i = 0; i < szARR_CNT_OF(dxy); i++) {
         int x = xy.x, y = xy.y;
         int nx = x + dxy[i][0], ny = y + dxy[i][1];
         if (1 > nx || 1 > ny || nx > n || ny > n) continue;
         if (a[nx][ny]) continue;
         a[nx][ny] = a[x][y] + 1;
         q.push(xy_t(nx, ny));
      }
   }

   printf("%d\n%d", a[x1][y1] - 1, a[x2][y2] - 1);
}

int main() {
   work1();
   return 0;
}
