//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}
#define N_MAX 1.037832e99
#define N_MIN -N_MAX
double f[10086];

void w1()
{
	int n;
	double avg=0, dif=N_MIN;
	scanf("%d", &n);
	//int mi=N_MAX, ma=N_MIN;
	double mi=N_MAX, ma=N_MIN;
	for(int i=0; i<n; i++){
		scanf("%lf", f+i);
		if(f[i] > ma) ma = f[i];
		if(f[i] < mi) mi = f[i];
		avg += f[i];
  }
  
	for(int i=0; i<n; i++){
		if(f[i] == ma){
		 f[i] = N_MIN;
		 break;
		}
  }
	for(int i=0; i<n; i++){
		if(f[i] == mi){
			f[i] = N_MIN;
			break;
		}
  }

  avg = (avg - ma - mi)/(n - 2);
	for(int i=0; i<n; i++){
		if(N_MIN != f[i]) dif = max(dif, abs(f[i] - avg));
  }

	printf("%.2lf %.2lf", avg, dif);
}

int main()
{
	w1();
  return 0;
}
