//SamXIAO

#include <cstdio>

#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

int a[200086];

void qs(int *x, int l, int r) {
   if (l >= r) return;
   int i = l, j = r, mid;
   int v = x[l + (r - l) / 2];
   while (i < j) {
      while (x[i] < v) i++;
      while (x[j] > v) j--;
      if (i <= j) {
         int t = a[i];
         a[i] = a[j];
         a[j] = t;
         i++, j--;
      }
   }
   qs(x, l, j);
   qs(x, i, r);
}

void work1() {
   int n, m;
   scanf("%d", &n);

   for (int i = 0, x; i < n; i++) {
      scanf("%d", &x);
      a[i] = x;
   }
   qs(a, 0, n - 1);
   //for (int i = 0; i < n; i++) printf(" %d%s", a[i], i == n - 1 ? "\n" : " ");

   int cnt = 0;
   for (int i = 0; i < n; i++) {
      if (0 == i) {
         printf("%d ", a[i]);
         cnt = 1;
      } else if (a[i] != a[i - 1]) {
         printf("%d\n%d ", cnt, a[i]);
         cnt = 1;
      } else if (a[i] == a[i - 1]) {
         cnt++;
      }
   }
   printf("%d", cnt);
}

int main() {
   //freopen("P1190_10.in", "r", stdin);
   work1();
   return 0;
}
