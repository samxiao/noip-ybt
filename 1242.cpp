//Sam.XIAO
#include <bits/stdc++.h>
using namespace std;

// 两位小数的double * 100 后转化为整数有溢出???

//int a[100085]; // 12ok, 1wa@4
double a[100085]; // AC
int n, k;
inline long long check(int expL) {
   long long res = 0;
   for (int i = 0; i < n; i++) {
      res += a[i] / expL;
   }
   return res;
}

void work1() {
   scanf("%d%d", &n, &k);
   long long tot = 0, ans = 0;
   for (int i = 0; i < n; i++) {
      double x;
      scanf("%lf", &x);
      a[i] = x * 100;
      tot += a[i];
   }
   sort(a, a + n);
   int l = 0, r = a[n - 1], mid;
   while (l <= r) {
      mid = l + (r - l) / 2;
      if (0 == mid) break;
      long long actK = check(mid);
      if (actK < k) {
         r = mid - 1;
      } else {
         if (ans < mid) ans = mid;
         l = mid + 1;
      }
   }
   printf("%.2lf", ans / 100.0);
}

int main() {
   work1();
   return 0;
}