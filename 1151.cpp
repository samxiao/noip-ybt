//SamXIAO
#include <bits/stdc++.h>
using namespace std;

bool judge(int x)
{
	for(int i = 2; i*i <= x; i++){
		if(0 == x % i) return false;
	}
	return true;
}
void w1()
{
	int n, cnt = 0;
	scanf("%d", &n);
	for(int i = 2; i <= n; i++){
		if(judge(i)) cnt++;
	}
	printf("%d", cnt);
}

int main()
{
	w1();
  return 0;
}
