#include <bits/stdc++.h>
using namespace std;

struct ele_t
{
   int dat, no;
   ele_t() {}
   ele_t(int dat, int no) : dat(dat), no(no) {}
   friend bool operator<(ele_t e1, ele_t e2)
   {
      return e1.dat < e2.dat;
   }
   friend bool operator>(ele_t e1, ele_t e2)
   {
      return e1.dat > e2.dat;
   }
};

bool flag[1500 * 1000 + 3];

void test1()
{
   priority_queue<ele_t, vector<ele_t>, greater<ele_t> > h1;
   h1.push(ele_t(100, 1));
   h1.push(ele_t(10, 1));
   h1.push(ele_t(120, 1));
   h1.push(ele_t(30, 1));
   h1.push(ele_t(130, 1));

   while (h1.size())
   {
      printf("%d  \n", h1.top().dat);
      h1.pop();
   }
}

void test2()
{
   priority_queue<ele_t, vector<ele_t>, less<ele_t> > h1;
   h1.push(ele_t(100, 1));
   h1.push(ele_t(10, 1));
   h1.push(ele_t(120, 1));
   h1.push(ele_t(30, 1));
   h1.push(ele_t(130, 1));

   while (h1.size())
   {
      printf("%d  \n", h1.top().dat);
      h1.pop();
   }
}
void work()
{
   priority_queue<ele_t, vector<ele_t>, greater<ele_t> > h1; // little root
   priority_queue<ele_t, vector<ele_t>, less<ele_t> > h2;

   int n, m, cnt = 0;
   //freopen("bill0.in", "r", stdin);
   //freopen("bill0.ans", "w", stdout);
   scanf("%d", &n);
   for (int i = 0; i < n; ++i)
   {
      scanf("%d", &m);
      int x;
      for (int j = 0; j < m; j++)
      {
         scanf("%d", &x);
         ele_t dat(x, cnt++);
         h1.push(dat);
         h2.push(dat);
      }
      ele_t x1, x2;
      x2 = h2.top();
      while (flag[x2.no] && h2.size()) h2.pop(), x2 = h2.top();
      flag[x2.no] = true;

      x1 = h1.top();
      while (flag[x1.no] && h1.size()) h1.pop(), x1 = h1.top();
      flag[x1.no] = true;

//      printf("%d %d\n", x1, x2); //!!! 本行在WINDOWS DEV C++中和下面一行结果相同，但在LINUX中编译有告警，运行结果错误。
      printf("%d %d\n", x1.dat, x2.dat);
      h1.pop();
      h2.pop();
   }
}
/*
5
3 6 7 1
3 8 8 8
3 8 8 8
3 8 8 8
3 8 8 8


1 7
6 8
8 8
8 8
8 8
*/
int main()
{
   
   work();
   return 0;
}
