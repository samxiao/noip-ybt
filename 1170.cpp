//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N 10000

class HPNum {
  private:
   struct num_t {
      int x[10086];
      int len;
   };
   num_t nums;

   static void str2num(char *s, num_t *num) {
      int pos = 0;
      while (s[pos] == '0' && pos < strlen(s)) pos++;
      num->len = strlen(s) - pos;
      for (int i = strlen(s) - 1, j = 0; i >= pos; i--, j++)
         num->x[j] = s[i] - '0';
   }
   void clear() {
      memset(nums.x, 0, sizeof(nums.x));
   }

  public:
   HPNum(char *a1) {
      clear();
      str2num(a1, &nums);
   }

   HPNum(num_t b) {
      clear();
      nums.len = b.len;
      for (int i = 0; i < b.len; i++)
         nums.x[i] = b.x[i];
   }

   HPNum() {
      clear();
   }

   void operator=(const HPNum b) {
      clear();
      nums.len = b.nums.len;
      for (int i = 0; i < b.nums.len; i++)
         nums.x[i] = b.nums.x[i];
   }

   HPNum operator-(const HPNum b) {
      HPNum x1(nums), x3;

      num_t *c = &x3.nums;
      int lc = c->len = max(b.nums.len, x1.nums.len);

      for (int i = 0; i < lc; i++) {
         x3.nums.x[i] += x1.nums.x[i] - b.nums.x[i];
         if (x3.nums.x[i] < 0) x3.nums.x[i + 1]--, x3.nums.x[i] += 10;
      }
      if (x3.nums.x[lc]) c->len++;

      return x3;
   }
   HPNum operator*(const HPNum b) {
      HPNum const x1(nums);
      HPNum x3;

      for (int i = 0; i < x1.nums.len; i++) {
         for (int j = 0; j < b.nums.len; j++) {
            x3.nums.x[i + j] += x1.nums.x[i] * b.nums.x[j];
         }
      }
      int pos = 0;
      while (pos < szCNT_OF(x3.nums.x)) {
         x3.nums.x[pos + 1] += x3.nums.x[pos] / 10;
         x3.nums.x[pos] %= 10;
         pos++;
      }

      int cnt = szCNT_OF(x3.nums.x) - 1;
      while (!x3.nums.x[cnt]) {
         cnt--;
      }
      x3.nums.len = cnt + 1;
      return x3;
   }
   void print() {
      int pos = nums.len;
      while (pos >= 0 && nums.x[pos] == 0) pos--;
      for (int i = pos; i >= 0; i--)
         printf("%d", nums.x[i]);
   }

   ~HPNum() {}
};

long long pw2(int n) {
   long long r = 1;
   while (n-- > 0) {
      r *= 2;
   }

   return r;
}

void work1() {
   char s1[10086] = {0}, s2[10086] = {0};
   int n;
   memset(s1, 0, sizeof(s1));
   memset(s2, 0, sizeof(s2));
   scanf("%d", &n);
#define MAXMM 60
   if (n > 10000) printf("n too big");
   sprintf(s1, "1");
   HPNum a(s1);
   while (n > 0) {
      sprintf(s2, "%lld", pw2(n < MAXMM ? n : MAXMM));
      HPNum b(s2), c;
      a = a * b;
      n -= MAXMM;
   }

   a.print();
}

int main() {
   work1();
   return 0;
}
