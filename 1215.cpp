#include <bits/stdc++.h>
using namespace std;

char a[102][102];
bool vi[102][102];
int ans;
int x1, y11, x2, y2;
void bfs(int n, int x, int y, int stp) {
   vi[x][y] = true;
   //printf("  %d%d:%c\n", x, y, a[x][y]);
   if (ans) return;
   //if ('#' == a[x][y]) return;
   if (x == x2 && y == y2 && '.' == a[x][y]) {
      ans++;
      return;
   }
   int dxy[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
   for (int i = 0; i < 4; i++) {
      int nx = x + dxy[i][0], ny = y + dxy[i][1];
      if (!vi[nx][ny] && '.' == a[nx][ny] && 0 <= ny && ny < n && 0 <= nx && nx < n) {
         //vi[nx][ny] = true;
         bfs(n, nx, ny, stp + 1);
         //vi[nx][ny] = false;
      }
   }
}

void work1() {
   int n, t;
   scanf("%d", &t);
   while (t--) {
      scanf("%d", &n);
      memset(a, 0, sizeof(a));
      memset(vi, 0, sizeof(vi));
      for (int i = 0; i < n; i++) scanf("%s", a[i]);
      scanf("%d%d%d%d", &x1, &y11, &x2, &y2);
      if ('#' == a[x1][y11] || '#' == a[x2][y2]) {
         printf("NO\n");
      } else {
         ans = 0;
         bfs(n, x1, y11, 1);
         printf("%s\n", ans ? "YES" : "NO");
      }
   }
}
int main() {
   work1();
   return 0;
}
