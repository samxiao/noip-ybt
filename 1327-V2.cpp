//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char a[10008];
int posSpace, step;
//按书上代码思路重写，这个思路更清晰
void output() { printf("step%2d:%s\n", step++, a); }
void init(int n) {
   for (int i = 0; i < n; i++) a[i] = 'o', a[i + n] = '*';
   a[2 * n] = a[2 * n + 1] = '-';
   posSpace = 2 * n;
   output();
}

void move2(int from) {
   a[posSpace] = a[from];
   a[posSpace + 1] = a[from + 1];
   a[from] = a[from + 1] = '-';
   posSpace = from;
   output();
}

void getans(int n) {
   if (4 >= n) {
      move2(3);
      move2(7);
      move2(1);
      move2(6);
      move2(0);
      return;
   }

   move2(n - 1);
   move2(2 * n - 2);
   getans(n - 1);
}

void work1() {
   int n;
   scanf("%d", &n);
   init(n);
   getans(n);
}

int main() {
   work1();
   return 0;
}
