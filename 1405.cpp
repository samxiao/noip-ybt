//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define MAXNN 10086
bool isprime(int x){
	if(1 >= x) return false;
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
 return true;
}

int f[MAXNN], cnt;
void gen()
{
	for(int x = 2; x <= MAXNN; x++)
		if(isprime(x)) f[cnt++] = x;
}

void work1()
{
	int s, ans = 0;
	gen();
	scanf("%d", &s);
	for(int i = 0; i <= cnt && f[i] < s; i++){
		int x;
		if(isprime(s - f[i])){
			x = f[i] * (s - f[i]);
			if(ans < x) ans = x;
		}
	}
	printf("%d", ans);
}

int main()
{
	work1();
  return 0;
}
