//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int getsum(int x)
{
	int ret = 0;
	for(int i = 1; i < x; i++)
		if(0 == x % i) ret += i;
	return ret;
}

void w1()
{
	for(int i = 1;   ; i++){
		int b = getsum(i);
		if(getsum(b) == i && b != i){
			printf("%d %d\n", min(i, b), max(i, b));
			break;
		}
	}
}

int main()
{
	w1();
  return 0;
}
