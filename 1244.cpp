//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

int a[1000085];
long long ans;

// luogu p1190. 注意m=1
void work1() {
   int n, m;
   scanf("%d", &n);

   for (int i = 0, x; i < n; i++) {
      scanf("%d", &x);
      a[i] = x;
   }
   long long q;
   scanf("%lld", &q);

   sort(a, a + n);
   for (int i = 0; i < n; i++) {
      int l = i + 1, r = n - 1, mid;
      while (l <= r) {
         mid = l + (r - l) / 2;
         //printf("%d[%d]:%d %d %d\n", i, a[i], l, r, mid);
         long long q2 = a[mid] + a[i];
         if (q2 == q) {
            printf("%d %d", a[i], a[mid]);
            return;
         } else if (q2 > q) {
            r = mid - 1;
         } else {
            l = mid + 1;
         }
      }
   }
   printf("No");
}

int main() {
   //freopen("P1190_10.in", "r", stdin);
   work1();
   return 0;
}
