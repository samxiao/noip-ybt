//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 102  // 1 * 1000 * 1000 + 86
#define MODN 32767

long long f[MAX_LEN][MAX_LEN];
void HorseMark(int n, int m, int mx, int my) {
   int dxy[][2] = {
       {-2, -1},
       {2, -1},
       {-2, 1},
       {2, 1},
       {-1, -2},
       {1, -2},
       {-1, 2},
       {1, 2}};
   for (int i = 0; i < szCNT_OF(dxy); i++) {
      int x1 = mx + dxy[i][0], y1 = my + dxy[i][1];
      if (0 <= x1 && x1 <= n && 0 <= y1 && y1 <= m) f[x1][y1] = -1;
   }
   f[mx][my] = -1;
}

void dt(int n, int m, int mx, int my) {
   HorseMark(n, m, mx, my);
   for (int i = 0; i <= n; i++) {
      if (-1 == f[i][0]) break; // 最边上的马控制点后的步数为0而不是1
      f[i][0] = 1;
   }
   for (int i = 0; i <= m; i++) {
      if (-1 == f[0][i]) break;
      f[0][i] = 1;
   }
   for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
         if (-1 != f[i][j]) {
            if (-1 != f[i - 1][j]) f[i][j] = f[i - 1][j];
            if (-1 != f[i][j - 1]) f[i][j] += f[i][j - 1];
         }
      }
   }
}

void work1() {
   int n, m, cx, cy;
   scanf("%d%d%d%d", &n, &m, &cx, &cy);
   dt(n, m, cx, cy);
   printf("%lld", f[n][m]);
}

int main() {
   work1();
   return 0;
}
