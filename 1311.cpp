//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAXLEN 5000
#define MAX_LEN 10 * 1000 * 1000 + 86
int n, dat[MAX_LEN], tmp[MAX_LEN];
//int ans;
long long ans;

void msort(int l, int r) {
   if (l >= r) return;
   int mid = (l + (r - l) / 2);
   msort(l, mid);
   msort(mid + 1, r);
   int i = l, j = mid + 1, t = l;
   while (i <= mid && j <= r) {
      if (dat[i] <= dat[j]) {
         tmp[t++] = dat[i++];
      } else {
         tmp[t++] = dat[j++];
         ans += mid - i + 1;
      }
   }
   while (i <= mid) tmp[t++] = dat[i++];
   while (j <= r) tmp[t++] = dat[j++];
   for (int i = l; i <= r; i++) dat[i] = tmp[i];
}

void work1() {
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      scanf("%d", &dat[i]);
   }
   msort(0, n - 1);
   //for (int i = 0; i < n; i++) printf("%d ", dat[i]);
   printf("%lld", ans);
}
/*
10
7 3 1 6 5 9 2 4 8 10

16
*/
int main() {
   work1();
   return 0;
}
