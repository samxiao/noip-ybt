#include <bits/stdc++.h>
using namespace std;
#define PI 3.14159

int main()
{
  double a, b ,c,x,d;
  scanf("%lf", &a  );
  printf("%.4lf %.4lf %.4lf", 2*a, 2*a*PI, PI*a*a);
  return 0;
}
