#include <bits/stdc++.h>
using namespace std;

int a[100085];
int find(int n, int x) {
   int res = -1, l = 0, r = n - 1, mid;
   while (l < r) {
      mid = l + (r - l) / 2;
      if (a[mid] == x) {
         return x;
      } else if (a[mid] > x) {
         r = mid - 1;
      } else {
         l = mid + 1;
      }
   }
   res = a[l];
   
   if (l > 0       && abs(a[l - 1] - x) < abs(res - x)) res = a[l - 1];
   if (l < (n - 1) && abs(a[l + 1] - x) < abs(res - x)) res = a[l + 1];
   return res;
}
 
void work1() {
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) scanf("%d", a + i);
   int m;
   scanf("%d", &m);
   for (int i = 0; i < m; i++) {
      int x;
      scanf("%d", &x);
      printf("%d\n", find(n, x));
   }
}

int main() {
   work1();
   return 0;
}
