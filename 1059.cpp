#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int x, y, n;
	double ans=0;
	scanf("%d",  &n );
	for(int i=0; i<n; i++){
		scanf("%d", &x);
		ans += x;
	}
	printf("%.2lf", ans / n);
  return 0;
}
