//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int ans, n, m, cap;

#define szARR_CNT_OF(x) sizeof(x) / sizeof(x[0])
#define TO_BOOL(x, mask) (x & mask) != 0
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
struct room_t {
   int x, y;
   bool w, e, n, s, flag;
   room_t(){};
   room_t(int x, int y, int v) : x(x), y(y) {}
   void setWall(int v) {
      w = TO_BOOL(v, 1);
      n = TO_BOOL(v, 2);
      e = TO_BOOL(v, 4);
      s = TO_BOOL(v, 8);
   }
};
room_t a[108][106];

struct xy_t {
   int x, y;
   xy_t(int x, int y) : x(x), y(y) {}
};

queue<xy_t> q;

bool isIn(int x, int y) {
   return 1 <= x && 1 <= y && x <= n && y <= m;
}

int maxx = 0;

void bfs(int x, int y) {
   if (a[x][y].flag) return;
   a[x][y].flag = true;
   ans++;
   cap = 0;
   q.push(xy_t(x, y));
   while (!q.empty()) {
      cap++;
      xy_t rm = q.front();
      q.pop();
      x = rm.x, y = rm.y;
      DBG_PRINT("%d,%d ", x, y);
      int nx, ny;
      nx = x, ny = y - 1;  // west
      if (isIn(nx, ny) && !a[nx][ny].flag && !a[x][y].w && !a[nx][ny].e) q.push(xy_t(nx, ny)), a[nx][ny].flag = true;
      nx = x, ny = y + 1;  // east
      if (isIn(nx, ny) && !a[nx][ny].flag && !a[x][y].e && !a[nx][ny].w) q.push(xy_t(nx, ny)), a[nx][ny].flag = true;
      nx = x + 1, ny = y;  // s
      if (isIn(nx, ny) && !a[nx][ny].flag && !a[x][y].s && !a[nx][ny].n) q.push(xy_t(nx, ny)), a[nx][ny].flag = true;
      nx = x - 1, ny = y;  // n
      if (isIn(nx, ny) && !a[nx][ny].flag && !a[x][y].n && !a[nx][ny].s) q.push(xy_t(nx, ny)), a[nx][ny].flag = true;
   }
   DBG_PRINT("\n");
   if (maxx < cap) maxx = cap;
}

void work1() {
   scanf("%d%d", &n, &m);
   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= m; j++) {
         int x;
         scanf("%d", &x);
         a[i][j].setWall(x);
      }

   for (int i = 1; i <= n; i++)
      for (int j = 1; j <= m; j++) bfs(i, j);

   printf("%d\n%d", ans, maxx);
}

int main() {
   work1();
   return 0;
}
/*
测试点	结果	内存	时间
测试点1	答案正确	472KB	6MS
测试点2	答案正确	464KB	5MS
测试点3	答案错误	468KB	6MS
测试点4	答案错误	488KB	5MS
测试点5	答案错误	480KB	6MS
测试点6	内存超限	65616KB	0MS
测试点7	运行错误	65616KB	265MS
测试点8	答案正确	568KB	7MS
测试点9	答案错误	2988KB	52MS
*/