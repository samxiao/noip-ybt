//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 102  // 10 * 1000 * 1000 + 86

string dat[MAX_LEN];

//bool cmp(string x1, string x2) { return x1 < x2; }
void work1() {
   string s1;
   int cnt = 0;
   while (cin >> s1) {
      dat[cnt++] = s1;
   };

   sort(dat, dat + MAX_LEN);
   s1 = "";
   for (int i = 0; i < MAX_LEN; i++) {
      if (s1 != dat[i]) cout << dat[i] << endl;
   }
}

int main() {
   work1();
   return 0;
}
