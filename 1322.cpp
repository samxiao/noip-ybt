#include <bits/stdc++.h>
using namespace std;

int a[1086];
int f[1086];
void work1() {
   int x, n = 0;
   while (scanf("%d", &x) != EOF) {
      a[n++] = x;
   }
   int ans = 1;
   f[0] = a[0];
   for (int i = 1; i < n; i++) {
      bool isKilled = false;
      for (int j = 0; j < ans; j++) {
         if (a[i] <= f[j]) {
            isKilled = true;
            f[j] = a[i];
            break;
         }
      }
      if (!isKilled) {
         f[ans++] = a[i];
      }
   }
   printf("%d", ans);
}

int main() {
   work1();
   return 0;
}