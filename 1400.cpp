//SamXIAO
#include <bits/stdc++.h>
using namespace std;

char s1[1000002];

bool cmp(char *s, char *d, int from, int to)
{
	bool isEq = true;
	int l1 = strlen(s), l2 = to - from + 1;
	if(l1 != l2) return false;
	for(int i=0; i<l1; i++){
		if(toupper(s[i]) != toupper(d[i+from])) return false;
	}
	return true;
}

//读错题意。 输出的是字符位置而不是第几个单词
void w1()
{
	int n, pos=0, cnt = 0, ans = -1;
	char dest[100000]={0} ;
	scanf("%s%*[\r\n]%[a-zA-Z ]", dest, s1);
	int l = strlen(s1), from = -1, to= -2;
	for(int i = 0; i < l; i++){
		char ch = s1[i];
		if(' ' == ch){
			if(0 <= to && cmp(dest, s1, from, to)) {
				if(!cnt) ans = from;
				cnt++;
			}
			from = -1;
			to = -2;
		} else {
			if(-1 == from) from = i;
			to = i;
		}
	}
	if(0 <= to){
		if(cmp(dest, s1, from, to)) {
			if(!cnt) ans = from;
			cnt++;
		}
	}
	if(cnt) printf("%d %d", cnt ,ans);
	else printf("-1");
}

int main()
{
	w1();
  return 0;
}
