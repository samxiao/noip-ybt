#include <bits/stdc++.h>
using namespace std;

void GenPrefix(char const *pat, int *nv) {
   int i = 0, j = -1, m = strlen(pat);
   nv[0] = -1;
   while (i < m) {
      if (-1 == j || pat[i] == pat[j]) {
         nv[++i] = ++j;
         //if (pat[i] == pat[j]) nv[i] = nv[j];
      } else {
         j = nv[j];
      }
   }
}

int KMP(char const *text, char const *pat, int *pr) {
   int i = 0, j = 0, m = strlen(pat), n = strlen(text);
   while (i < n) {
      if (-1 == j || text[i] == pat[j]) {
         i++;
         j++;
         if (j == m) return i - j;
      } else {
         j = pr[j];
      }
   }
   return -1;
}
// clang-format on
int prefix[1005];
int main() {
   string t, p;
   while (true) {
      cin >> t;
      if (t == "#") break;
      cin >> p;
      memset(prefix, 0, sizeof(prefix));
      GenPrefix(p.c_str(), prefix);
      for (int i = 0; i < strlen(p.c_str()); i++) {
         printf("%d%s", prefix[i], i < strlen(p.c_str()) - 1 ? " " : "\n");
      }
      int ans = 0, pos = 0;
      while (pos < strlen(t.c_str())) {
         int res = KMP(&t.c_str()[pos], p.c_str(), prefix);
         if (-1 == res) break;
         pos += strlen(p.c_str()) + res;
         ans++;
      }
      printf("%d\n", ans);
   }
}

/*
ababc
ababc
-1 -1 0 1 -1
1

ababc
ababc
-1 0 0 1 2
*/