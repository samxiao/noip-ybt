//SamXIAO
#include <cstdio>
#include <cstring>
 
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

char b[100], f[100];

void dfs(char *s, int len, int pos) {
   if (pos >= len) {
      for (int i = 0; i < len; i++) printf("%c", b[i]);
      printf("\n");
      return;
   }
   for (int i = 0; i < len; i++) {
      if (!f[i]) {
         f[i] = 1;
         b[pos] = s[i];
         dfs(s, len, pos + 1);
         f[i] = 0;
      }
   }
}

void work1() {
   char a[100];
   scanf("%s", a);
   dfs(a, strlen(a), 0);
}

int main() {
   work1();
   return 0;
}
