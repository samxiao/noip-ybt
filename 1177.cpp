//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAXLEN 5000
#define MAX_LEN 10 * 1000 * 1000 + 86

int dat[MAX_LEN], tmp[MAX_LEN];
long long ans;

void msort(int l, int r) {
   if (l >= r) return;
   int mid = (l + (r - l) / 2);
   msort(l, mid);
   msort(mid + 1, r);
   int i = l, j = mid + 1, t = l;
   while (i <= mid && j <= r) {
      if (dat[i] <= dat[j]) {
         tmp[t++] = dat[i++];
      } else {
         tmp[t++] = dat[j++];
         //ans += mid - i + 1;
      }
   }
   while (i <= mid) tmp[t++] = dat[i++];
   while (j <= r) tmp[t++] = dat[j++];
   for (int i = l; i <= r; i++) dat[i] = tmp[i];
}

void work1() {
   int n, cnt = 0;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      int x;
      scanf("%d", &x);
      if (x % 2) dat[cnt++] = x;
   }
   msort(0, cnt - 1);
   for (int i = 0; i < cnt; i++) printf("%d%s", dat[i], i == cnt - 1 ? "" : ",");
}

int main() {
   work1();
   return 0;
}
