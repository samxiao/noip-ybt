//200123 3h AC.

#include <bits/stdc++.h>

using namespace std;
 
#define szARR(x) sizeof(x)/sizeof(x[0])
#define INT64_t long long 
#if 0
#define DBG_PRINT printf
#else
#define DBG_PRINT 
#endif

INT64_t calculate(char op, INT64_t x1, INT64_t x2)
{
   INT64_t r=0;
   switch(op)
   {
      case '+':
         r = x1 + x2;
         break;
      case '-':
         r = x1 - x2;
         break;
      case '*':
         r = x1 * x2;
         break;
      case '/':
         r = x1 / x2;
         break;
      case '^':
         r = pow(x1,  x2);
         break;
      case '%':
         r = x1 % x2;
         break;
      default:
         break;
   }
   return r;
}

int GetPriBesideStk(char ch)
{
   char opset[][5]={")", "+-", "*/%", "^", "("};
   for(int i=0; i<szARR(opset); i++)
      for(int j=0; j<strlen(opset[i]); j++)
         if(ch == opset[i][j]) return i;
   return 0;
}

int GetPriInnerStk(char ch)
{
   char opset[][5]={"(", "+-", "*/%", "^", ")"};
   for(int i=0; i<szARR(opset); i++)
      for(int j=0; j<strlen(opset[i]); j++)
         if(ch == opset[i][j]) return i;
   return 0;
}

int CmpOp(char opOut, char opInner)
{
   int priO = GetPriBesideStk(opOut);
   int priI = GetPriInnerStk(opInner);
   if(priO >  priI) return 1;
   if(priO == priI) return 0;
   return -1;
}

stack<INT64_t> num;
stack<char> op;


inline INT64_t CalcOnceInStack()
{
   INT64_t x1, x2, r;
   x2 = num.top(), num.pop();
   x1 = num.top(), num.pop();
   char opCur = op.top();
   op.pop();
   r = calculate(opCur, x1, x2);
   DBG_PRINT("%lld%c%lld=%lld\n", x1, opCur, x2, r);
   num.push(r);
}

#define SPEC_CHAR_SUB '#'
bool expIsError = false;

bool CharIsError(char const checkNeed, char const *sError)
{
   //char tail[]="(+-*/^";
   for(int i=0; i<strlen(sError); i++)
      if(checkNeed == sError[i]) return true;
   return false;
}

bool NextCharIsError(char const chCur, const char chNxt)
{
   bool isError = false;
   
   if(     '(' == chCur) isError = isError ||  CharIsError(chNxt, "+*/^");
   else if(')' == chCur) isError = isError ||  CharIsError(chNxt, "$");
   else                  isError = isError ||  CharIsError(chNxt, "+*/^-)");
   return isError;
}

// 检查括号匹配
bool CheckBracketMatch(char const *s, int len)
{
   stack<char> stk;
   for(int i=0; i<len; i++){
      char ch = s[i];
      if(')' == ch){
         if(!stk.empty()){
            if(stk.top() != '(') return false;
            stk.pop();
         }
         else return false;
      }
      else if('(' == ch) stk.push(ch);
   }
   return stk.empty();
}

INT64_t CalcMiddleExp(char const *s, INT64_t len)
{
   INT64_t x = 0, symb = 1;
   char ch, chPrev = SPEC_CHAR_SUB;
   bool hasNum = false;
   expIsError = expIsError || CharIsError(s[len - 1], "(+-*/^"); // tail
   expIsError = expIsError || CharIsError(s[0], ")+*/^");        // head
   expIsError = expIsError || !CheckBracketMatch(s, len);
   if(expIsError) return 0;
   for(int i=0; i<len; i++)
   {
      ch = s[i];
      if(isdigit(ch)) x = x * 10 + ch - '0', hasNum = true;
      else
      {
         if(i < len - 1) expIsError = expIsError || NextCharIsError(ch, s[i + 1]);           
         if(expIsError) return 0;
         if(hasNum) num.push(x * symb);
         hasNum = false, x = 0, symb = 1;
         if('-' == ch && (SPEC_CHAR_SUB == chPrev || '(' == chPrev)) symb = -1;
         else
         {
            if(!op.empty())
            {
               char opIn = op.top();
               if(')' == ch)
               {
                  while(op.top() != '(') CalcOnceInStack();
                  op.pop();
               }
               else if(CmpOp(ch, opIn) <= 0) CalcOnceInStack();
            }
            if(')' != ch) op.push(ch);
         }
      }
      chPrev = ch;
   }
   if(hasNum) num.push(x);
   while(!op.empty()) CalcOnceInStack();
   return num.top();
}

int main()
{
   string s;
   INT64_t ans;
   cin >> s;
   
   ans=CalcMiddleExp(&s[0], s.length() - 1);
   if(expIsError)  printf("NO");
   else printf("%lld", ans);

   return 0;
}

/*
1+2*8-9@
8

(5*(5-9))+3-9-4@
-30 

-4+5-9+10/*2@
NO
*/