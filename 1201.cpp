#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

int f[1000], cnt;

int fib(int x)
{
	if(1 == x || 2 == x) return 1;
	return fib(x - 1) + fib(x - 2);
}

void work1()
{
	int n, x;
	scanf("%d", &n);

	for(int i = 0; i < n; i++){
		scanf("%d", &x);
		printf("%d\n", fib(x));
	}
}

int main()
{
   work1();
   return 0;
}

