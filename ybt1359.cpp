#include <bits/stdc++.h>
using namespace std;

#if 0 //#ifndef ONLINE_JUDGE
#define DBG_PRINT printf
#else
#define DBG_PRINT
#endif
#define MAXN 10
/*
1359：围成面积
SamXIAO 200126
*/

int dxy[][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
int dat[MAXN][MAXN], n = 10;
void dfs(int x, int y)
{
   if (x >= 0 && y >= 0 && x < n && y < n)
   {
      if (0 == dat[x][y])
      {
         dat[x][y] = 1;
         dfs(x + 1, y);
         dfs(x - 1, y);
         dfs(x, y + 1);
         dfs(x, y - 1);
      }
   }
}

int main()
{
   for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
         cin >> dat[i][j];

   for (int i = 0; i < n; i++)
   {
      dfs(i, 0);
      dfs(0, i);
      dfs(n - 1, i);
      dfs(i, n - 1);
   }

   int ans = 0;
   for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
         if (0 == dat[i][j])
            ans++;

   printf("%d", ans);
   return 0;
}

/*
 
*/
