#include <bits/stdc++.h>
using namespace std;
//200129 trie dictory tree
#define MAXN 32002
int dat[MAXN][26];
bool isW[MAXN];

int tot;
void insert(char *s){
   int u = 0;  // u 和 tot的初始值必须相同!
   for(int i=0; i<strlen(s); i++){
      int c=s[i] - 'A';
      if(!dat[u][c]) dat[u][c] = ++tot;
      u = dat[u][c];
   }
   isW[u] = true;
}

bool Find(char *s){
   int u = 0;
   for(int i=0; i<strlen(s); i++){
      int c = s[i] - 'A';
      if(!dat[u][c]) return false;
      u = dat[u][c];
   }
   return true;
}


int main()
{
   int n = 0, m, p, c;
   char s1[MAXN];
   while (EOF != scanf("%s", s1)){
      insert(s1);
   }
      
   printf("%d", tot + 1);
   return 0;
}
