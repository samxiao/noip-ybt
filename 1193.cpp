//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 10002  // 1 * 1000 * 1000 + 86
#define MODN 32767

int f[MAX_LEN] = {1, 1, 2};

void work1() {
   int n, k;
   scanf("%d", &n);

   for (int i = 2; i <= n; i++) f[i] = f[i - 1] + f[i - 2];

   printf("%d", f[n]);
}

int main() {
   work1();
   return 0;
}
