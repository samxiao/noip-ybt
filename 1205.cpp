//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

void mv(int n, char a, char b, char z) {
   if (0 == n) return;
   mv(n - 1, a, z, b);
   printf("%c->%d->%c\n", a, n, b);
   mv(n - 1, z, b, a);
}

void work1() {
   int n;
   char a, b, z;
   scanf("%d %c %c %c", &n, &a, &b, &z);
   mv(n, a, b, z);
}

int main() {
   work1();
   return 0;
}
