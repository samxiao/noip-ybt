#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long
// P6038 合并果子 加强版 会TLE. 必须用手写堆+高精才能过.
#define MAXN 1000
#define MAXW 1000
int f[MAXW], w[MAXN], c[MAXN];
inline long long read()
{
   long long x=0;char ch=getchar();
   while(!isdigit(ch)){ch=getchar();}
   while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
   return x;
}
int main()
{
   int n, dat, ans = 0;
   priority_queue<int, vector<int>, greater<int> > h; // little root heap
//   priority_queue<int, vector<int>, less<int> > h; // big root heap
   scanf("%d", &n);
   for (int i = 0; i < n; i++)
   {
      dat = read();
      h.push(dat);
   }
   while (true)
   {
      int x;
      if (1 == h.size()) break;
      x =  h.top(), h.pop();
      x += h.top(), h.pop();
      ans += x;
      h.push(x);
   }
   printf("%d", ans);
   return 0;
}
