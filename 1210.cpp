//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

char f[1000];
int maxx;
void sch(int x, int n) {
   if (0 == x || x > n) return;
   while (0 == n % x) {
      f[x]++;
      n /= x;
      maxx = x;
   }
   sch(x + 1, n);
}

void work1() {
   int n;
   scanf("%d", &n);
   sch(2, n);
   for (int i = 1; i < 100; i++) {
      if (f[i]) {
         if (f[i] > 1) {
            printf("%d^%d", i, f[i]);
         } else {
            printf("%d", i);
         }
         if (i != maxx) printf("*");
      }
   }
}

int main() {
   work1();
   return 0;
}
