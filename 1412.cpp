//SamXIAO
#include <bits/stdc++.h>
using namespace std;


bool isA(int x)
{
	int cnt1 = 0, cnt0=0;
	while(x){
		if(x % 2) cnt1++;
		else cnt0++;
		x /= 2;
	}
	
	return cnt1 > cnt0;
}

void work1()
{
	int a=0, b=0;
	for(int i = 1; i <= 1000; i++)
	if(isA(i)) a++;
	else b++;
	printf("%d %d", a, b);
}

int main()
{
	work1();
  return 0;
}
