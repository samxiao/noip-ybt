//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAX_LEN 300  // 10 * 1000 * 1000 + 86

struct stu_t {
   int no, w, s, e;
   int total;
   stu_t() {}
   stu_t(int no, int wy, int sx, int eng) : no(no), w(wy), s(sx), e(eng) {
      total = w + s + e;
   }
   bool operator<(stu_t const b) {
      if (total == b.total) {
         if (w == b.w) return no < b.no;
         return w > b.w;
      }
      return total > b.total;
   }
};

stu_t dat[MAX_LEN];
bool cmp(stu_t x1, stu_t x2) { return x1 < x2; }
void work1() {
   int n;
   int no=1;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      int  w, s, e;
      scanf("%d%d%d", &w, &s, &e);
      dat[i] = stu_t(no++, w, s, e);
   }
   sort(dat, dat + n, cmp);
   for (int i = 0; i < 5; i++) printf("%d %d\n", dat[i].no, dat[i].total);
}

int main() {
   work1();
   return 0;
}
