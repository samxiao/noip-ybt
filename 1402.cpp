//SamXIAO
#include <bits/stdc++.h>
using namespace std;

#define MAXNN 10086

int cnt;
char s1[202];

char vigtbl[256][256];

void gen()
{
	for(char i = 0; i < 26; i++){
		for(char j = 0; j < 26; j++){
			vigtbl[i][j] = (i + j) % 26 + 'A';
		}
	}
#if 0
	for(char i = 0; i < 26; i++){
		for(char j = 0; j < 26; j++){
			printf("%c ", vigtbl[i][j]);
		}
		printf("\n");
	}
#endif
}

char vig_dec(char k, char c)
{
	for(char i = 0; i < 26; i++)
			if(vigtbl[toupper(k) - 'A'][i] == toupper(c))
				return isupper(c) ? 'A' + i : 'a' + i;
}

char m1[1086], c1[1086], k1[186];
void work1()
{
	scanf("%s%s", k1, c1);
	gen();
	int pos = 0;
	for(int i=0; i<strlen(c1); i++){
		printf("%c", vig_dec(k1[pos], c1[i]));
		if(++pos>=strlen(k1)) pos=0;
	}
}

int main()
{
	work1();
  return 0;
}
