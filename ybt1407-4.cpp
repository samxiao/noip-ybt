// AC@LG;  AC@YBT
#include <bits/stdc++.h>
using namespace std;
// lg: P1125 笨小猴
int main() {
   char const *ch;
   string s;
   cin >> s;
   ch = &s.c_str()[0];
   int n = strlen(ch), max = 0, min = 100;
   for (int i = 0; i < n; i++) {
      int t = 0;
      for (int j = 0; j < n; j++)
         if (ch[i] == ch[j]) t++;

      if (t > max) max = t;
      if (t < min) min = t;
   }
   int x = max - min;
   if (x >= 2) {
      int flag = 0;
      for (int i = 2; i <= sqrt(x); i++) {
         if (x % i == 0) {
            flag = 1;
            break;
         }
      }
      if (flag == 0)  cout << "Lucky Word" << endl << x;
      else            cout << "No Answer"  << endl << '0';
   } else             cout << "No Answer"  << endl << 0;
   
   return 0;
}
