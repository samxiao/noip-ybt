//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int getmax(int x)
{
	int ret=0;
	while(x){
		if(ret < x % 10 ) ret = x % 10;
		x /= 10;
	}
	
	return ret;
}

int nto10(int x, int base)
{
	int ret = 0, v=1;
	while(x){
		ret += (x % 10) * v;
		v *= base;
		x /= 10;
	}
	return ret;
}

void work1()
{
	int p, q, r;
	scanf("%d%d%d", &p, &q, &r);
	int minx = max(getmax(p), getmax(q));
	minx = max(getmax(r), minx) + 1;
	for(int i = minx; i <= 40; i++)
		if(nto10(p, i) * nto10(q, i) == nto10(r, i))
			printf("%d", i), r = -1;
	
	if(-1 != r) printf("0");
}

int main()
{
	work1();
  return 0;
}
