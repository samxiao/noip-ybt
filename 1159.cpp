//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int f(int x )
{
	if(2 == x || 1 == x) return x - 1;
	return f(x - 1) + f(x - 2);
}

void work1()
{
	int n;
	scanf("%d", &n);
	printf("%d", f(n));
}

int main()
{
	work1();
  return 0;
}
