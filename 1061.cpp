#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int n, a, sum=0;
	double x, ans=0;
	scanf("%d",  &n );
	for(int i=0; i<n; i++){
		scanf("%lf", &x);
		sum += x;
	}
	printf("%d %.5lf", sum, sum / (double)n);
  return 0;
}
