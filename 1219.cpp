#include <bits/stdc++.h>
using namespace std;

#define szARR_CNT(x) (sizeof(x) / sizeof(x[0]))
bool f[102][102];
int num, ans, n, m;
int dxy[][2] = {
    {1, 2},
    {1, -2},
    {-1, 2},
    {-1, -2},
    {2, 1},
    {2, -1},
    {-2, 1},
    {-2, -1}};

void search(int curx, int cury, int step) {
   if (step >= n * m) {
      ans++;
      return;
   }

   for (int i = 0; i < szARR_CNT(dxy); i++) {
      int nx = curx + dxy[i][0], ny = cury + dxy[i][1];
      if (0 <= nx && nx < n && 0 <= ny && ny < m) {
         if (!f[nx][ny]) {
            f[nx][ny] = true;
            search(nx, ny, step + 1);
            f[nx][ny] = false;
         }
      }
   }
}

void search(int curx, int cury) {
   if (num >= n * m) {
      ans++;
      return;
   }

   for (int i = 0; i < szARR_CNT(dxy); i++) {
      int nx = curx + dxy[i][0], ny = cury + dxy[i][1];
      if (0 <= nx && nx < n && 0 <= ny && ny < m) {
         if (!f[nx][ny]) {
            f[nx][ny] = true;
            num++;
            search(nx, ny);
            num--;
            f[nx][ny] = false;
         }
      }
   }
}

void work1() {
   int t, sx, sy;
   scanf("%d", &t);
   while (t--) {
      memset(f, 0, sizeof(f));
      num = 1, ans = 0;

      //scanf("%d%d", &n, &m, &sx, &sy);  // bug
      scanf("%d%d%d%d", &n, &m, &sx, &sy);
      f[sx][sy] = true;
      search(sx, sy);
      //search(sx, sy, 1);
      printf("%d\n", ans);
   }
}

int main() {
   work1();
   return 0;
}
