#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long
S64_t num[10000], top;

S64_t cacl(char op, S64_t x1, S64_t x2)
{
   S64_t r = 0;
   switch(op){
      case '+':
         r = x1 + x2;
         break;
      case '-':
         r = x1 - x2;
         break;
      case '*':
         r = x1 * x2;
         break;
      case '/':
         r = x1 / x2;
         break;
   }
   return r;
}

int main()
{
   char ch;  
   S64_t x = 0;
   bool hasNum = false;
   while(true) 
   {
      ch = getchar();
      if ('@' == ch) break;
      if (isdigit(ch))
      {
         x = x * 10 + ch - '0';
         hasNum = true;
      }
      else{
         if(hasNum) num[top++] = x;
         hasNum = false;
         x = 0;
         if ('+' == ch || '-' == ch || '*' == ch || '/' == ch)
         {
            S64_t x1, x2;
            x2 = num[--top];
            x1 = num[--top];
            num[top++] = cacl(ch, x1, x2);
         }
      }
   }

   printf("%lld", num[0]);
   return 0;
}