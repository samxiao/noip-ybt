#include <bits/stdc++.h>
using namespace std;

struct ele_t {
   int t, no;
};
ele_t a[1085];

bool cmp(ele_t x1, ele_t x2) { return x1.t < x2.t; }

void work1() {
   int n;
   scanf("%d", &n);
   for (int i = 1; i <= n; i++) {
      scanf("%d", &a[i].t);
      a[i].no = i;
   }
   sort(a + 1, a + n + 1, cmp);
   double ans = 0;
   for (int i = 1; i <= n; i++) {
      printf("%d ", a[i].no);
      ans += a[i].t * (n - i); // 最后一个人打水不算入等待时间。
   }
   ans /= n;
   printf("\n%.2lf", ans);
}

int main() {
   work1();
   return 0;
}