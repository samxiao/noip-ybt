//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 20  // 1 * 1000 * 1000 + 86

long long dg(int n, int k) {
   if (0 == n || 1 == k)
      return 1;
   else if (0 == k)
      return 0;
   else if (n < k)
      return dg(n, n);
   else
      return dg(n - k, k) + dg(n, k - 1);
}

void work1() {
   int t;
   scanf("%d", &t);

   while (t--) {
      int n, k;
      scanf("%d%d", &n, &k);
      printf("%lld\n", dg(n, k));
   }
}

int main() {
   work1();
   return 0;
}
