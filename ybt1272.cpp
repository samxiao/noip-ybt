#include <bits/stdc++.h>

using namespace std;

#define int64_t signed long long
#define uint64_t unsigned long long

struct ele_t {
   int w, c, g;
   ele_t(int w, int c, int g) : w(w), g(g), c(c) {}
};
vector<ele_t> g[100];
int f[10086];

void work_v1() {
   int v, n, t;
   scanf("%d%d%d", &v, &n, &t);
   for (int i = 1; i <= n; i++) {
      int w, c, p;
      scanf("%d%d%d", &w, &c, &p);
      g[p].push_back(ele_t(w, c, p));
   }

   for (int i = 1; i <= t; i++)
      for (int j = v; j >= 0; j--)
         for (int k = 0; k < g[i].size(); k++)
            if (j >= g[i][k].w) f[j] = max(f[j], f[j - g[i][k].w] + g[i][k].c);

   printf("%d", f[v]);
}

int main() {
   work_v1();
   return 0;
}