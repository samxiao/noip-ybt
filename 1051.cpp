#include <bits/stdc++.h>
using namespace std;
#define PI 3.1415926
#define   SQUARE(x) ((x)*(x))
//double SQUARE(double x) { return x*x;}

int main()
{
	int x1, y ;
	double x2, b;
	char c;
	scanf("%lf",  &x2);
	if(     0  <= x2 && x2 < 5)  b = -x2 + 2.5;
	else if(5  <= x2 && x2 < 10) b = 2 - 1.5 * (x2 - 3)*(x2 - 3);
	else if(10 <= x2 && x2 < 20) b = x2 / 2 - 1.5;
	printf("%.3lf", b);

  return 0;
}
