//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 5500  // 10 * 1000 * 1000 + 86

int dat[MAX_LEN], dat1[MAX_LEN];
bool cmp(int x1, int x2) { return x1 < x2; }
bool cmp1(int x1, int x2) { return x1 > x2; }
void work1() {
   int n, cnt0 = 0, cnt1 = 0;
   n = 10;
   for (int i = 0; i < n; i++) {
      int x;
      scanf("%d", &x);
      if (x % 2)
         dat1[cnt1++] = x;
      else
         dat[cnt0++] = x;
   }

   sort(dat, dat + cnt0, cmp);
   sort(dat1, dat1 + cnt1, cmp1);

   for (int i = 0; i < cnt1; i++) {
      printf("%d ", dat1[i]);
   }
   for (int i = 0; i < cnt0; i++) {
      printf("%d ", dat[i]);
   }
}

int main() {
   work1();
   return 0;
}
