//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define BASE_N_LEN 8
#define MAX_LEN 100// 10 * 1000 * 1000 + 86

struct stu_t {
   char name[100];
   int cj;
   stu_t() {
      memset(name, 0, sizeof(name));
   }
};

stu_t dat[MAX_LEN];
bool cmp(stu_t a, stu_t b) {
   if (a.cj == b.cj) {
      int l = min(strlen(a.name), strlen(b.name));
      for (int i = 0; i < l; i++) {
         if (a.name[i] != b.cj) {
            return a.name[i] < b.name[i];
         }
      }
      return true;
   }
   return a.cj > b.cj;
}
long long ans;

void work1() {
   int n;
   scanf("%d", &n);
   for (int i = 0; i < n; i++) {
      scanf("%s%d", dat[i].name, &dat[i].cj);
   }
   sort(dat, dat + n, cmp);
   for (int i = 0; i < n; i++) printf("%s %d\n", dat[i].name, dat[i].cj);
}

int main() {
   work1();
   return 0;
}
