//SamXIAO
#include <bits/stdc++.h>
using namespace std;

int f[102];
int num[100];
int cnt;
bool isprime(int x){
	for(int i = 2; i * i <= x; i++)
		if(0 == x % i) return false;
	return true;
}

void gen()
{
	for(int i = 2; i <= 100; i++){
		if(isprime(i)) num[cnt++] = i;
	}
}

void w1()
{
	gen();
	for(int i = 6; i <= 100; i += 2){
		for(int j = 0; j < cnt; j++){
			if(num[j] < i && isprime(i - num[j])){
				printf("%d=%d+%d\n", i, num[j], i - num[j]);
				break;
			}
		}
	}
}

int main()
{
	w1();
  return 0;
}
