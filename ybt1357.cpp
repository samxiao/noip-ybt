#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long
#define MAXX 1000 + 5
int a[MAXX], c[MAXX];
int top = 0, ap=1;

int main()
{
   int x;
   int n;
   char ch;
   cin >> n;
   bool isOk = true;
   for (int i = 1; i <= MAXX; i++) a[i] = i;
   for (int i = 0; i < n; i++)
   {
      cin >> x;
      if(top<=0) c[top++] = ap++;
      while(c[top - 1] != x && ap <MAXX)
      {
         c[top++] = ap++;
      }
      if(c[top - 1] != x){
         printf("NO");
         return 0;
      }
      top--;
   }
   printf("YES");
   return 0;
}