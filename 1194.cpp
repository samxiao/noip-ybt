//SamXIAO
#include <bits/stdc++.h>
using namespace std;
#define szCNT_OF(x) (sizeof(x) / sizeof(x[0]))
#define MAX_LEN 1002  // 1 * 1000 * 1000 + 86
#define MODN 32767

long long f[MAX_LEN][MAX_LEN];

void work1() {
   int n, m;
   scanf("%d%d", &m, &n);
   for (int i = 1; i <= m; i++) f[i][1] = 1;
   for (int i = 1; i <= n; i++) f[1][i] = 1;

   for (int i = 2; i <= m; i++) {
      for (int j = 2; j <= n; j++) {
         f[i][j] = f[i][j - 1] + f[i - 1][j];
      }
   }

   printf("%lld", f[m][n]);
}

int main() {
   work1();
   return 0;
}
