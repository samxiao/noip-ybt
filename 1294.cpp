#include <bits/stdc++.h>
using namespace std;

int w[3800], c[3800];
int f[13033];

int main() {
   int m, n;
   scanf("%d%d", &n, &m);
   for (int i = 1; i <= n; i++) scanf("%d%d", w + i, c + i);
   for (int i = 1; i <= n; i++)
      for (int j = m; j >= w[i]; j--)
         f[j] = max(f[j], f[j - w[i]] + c[i]);

   printf("%d", f[m]);
}