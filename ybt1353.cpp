#include <bits/stdc++.h>
using namespace std;

#define S64_t long long
#define U64_t unsigned long long
S64_t num[10000], top;
 

int main()
{
   char ch;  
   while(true) 
   {
      ch = getchar();
      if ('@' == ch) break;
      if ('(' == ch) top++;
      else if(')' == ch) top--;
   }

   printf("%s", 0 == top ? "YES" : "NO");
   return 0;
}