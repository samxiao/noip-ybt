#include <bits/stdc++.h>
using namespace std;

long long ans = 0;

long long f[100086];
void gen() {
   f[1] = 1, f[2] = 2;
   for (int i = 3; i <= 71; ++i) {
      f[i] = f[i - 2] + f[i - 1];
   }
}
int main() {
   int n;
   gen();

   while (~scanf("%d", &n)) {
      printf("%lld\n", f[n]);
   }

   return 0;
}
